-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: smartfleet
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_atividade` int(11) DEFAULT NULL,
  `id_tipo_manutencao` int(11) DEFAULT NULL,
  `complemento_manutencao` varchar(40) DEFAULT NULL,
  `transporte_vazio` varchar(80) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camada`
--

DROP TABLE IF EXISTS `camada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_camada` int(11) NOT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `excluido` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camada`
--

LOCK TABLES `camada` WRITE;
/*!40000 ALTER TABLE `camada` DISABLE KEYS */;
INSERT INTO `camada` VALUES (8,1,'Testes',1,'2017-09-11 03:50:03','2017-09-11 04:02:30'),(9,2,'aaa',1,'2017-09-11 04:03:26','2017-09-11 04:03:29');
/*!40000 ALTER TABLE `camada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camada_projeto`
--

DROP TABLE IF EXISTS `camada_projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camada_projeto` (
  `id_camada` int(11) NOT NULL,
  `id_projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camada_projeto`
--

LOCK TABLES `camada_projeto` WRITE;
/*!40000 ALTER TABLE `camada_projeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `camada_projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razaosocial` varchar(70) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `ie` varchar(20) DEFAULT NULL,
  `im` varchar(20) DEFAULT NULL,
  `endereco` varchar(80) DEFAULT NULL,
  `cidade` varchar(40) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `complemento` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Empresa XYZ','000.000-0001/000','IE','IM','Rua teste, jardim homologação','Curitiba','81000-000',NULL,'2017-06-27 17:18:49','2017-06-27 17:18:49');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_carga`
--

DROP TABLE IF EXISTS `local_carga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_carga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_local` int(11) DEFAULT NULL,
  `descricao` varchar(50) DEFAULT NULL,
  `latitude` tinytext,
  `longitude` tinytext,
  `complemento_manual` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_carga`
--

LOCK TABLES `local_carga` WRITE;
/*!40000 ALTER TABLE `local_carga` DISABLE KEYS */;
INSERT INTO `local_carga` VALUES (1,1,1,'Jazida Balança',NULL,NULL,0,'2017-06-27 17:41:34','2017-06-27 17:41:34'),(2,1,2,'Jazida Balança',NULL,NULL,0,'2017-06-27 17:41:34','2017-06-27 17:41:34'),(3,1,1,'Estaca','','',1,'2017-06-27 17:41:50','2017-07-31 08:21:35'),(4,1,2,'Estaca','123','321',0,'2017-06-27 17:41:50','2017-07-31 08:21:31'),(5,1,1,'Polimix',NULL,NULL,0,'2017-06-27 17:42:14','2017-06-27 17:42:14'),(6,1,2,'Polimix',NULL,NULL,0,'2017-06-27 17:42:14','2017-06-27 17:42:14'),(7,0,0,'','','',0,'2017-07-31 07:57:49','2017-07-31 07:57:49'),(8,0,0,'','','',0,'2017-07-31 07:57:55','2017-07-31 07:57:55'),(9,1,2,'teste cm v','3213','123213',1,'2017-08-07 02:20:29','2017-08-07 02:22:38'),(10,1,1,'teste cm n','321312','231321312',0,'2017-08-07 02:21:01','2017-08-07 02:21:01'),(11,1,1,'teste cm n','321312','231321312',0,'2017-08-07 02:21:11','2017-08-07 02:21:11'),(12,1,1,'teste cm n','321312','231321312',0,'2017-08-07 02:21:48','2017-08-07 02:21:48'),(13,0,0,'testeeeeeeeee','','',0,'2017-08-07 04:26:37','2017-08-07 04:26:37'),(14,0,0,'','','',0,'2017-09-11 01:24:25','2017-09-11 01:24:25');
/*!40000 ALTER TABLE `local_carga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_carga_sub`
--

DROP TABLE IF EXISTS `local_carga_sub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_carga_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_local_carga` int(11) DEFAULT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `latitude` tinytext,
  `longitude` tinytext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_carga_sub`
--

LOCK TABLES `local_carga_sub` WRITE;
/*!40000 ALTER TABLE `local_carga_sub` DISABLE KEYS */;
INSERT INTO `local_carga_sub` VALUES (6,11,'3055-3060','-22,71682628','-49,44732694\r','2017-09-11 01:29:55','2017-09-11 01:29:55'),(7,11,'3060-3065','-22,71750158','-49,44797328','2017-09-11 01:29:55','2017-09-11 01:29:55'),(8,11,'\r3065-3070','-22,71817687','-49,44861963','2017-09-11 01:29:55','2017-09-11 01:29:55'),(9,11,'\r3070-3075','-22,71885217','-49,44926598','2017-09-11 01:29:55','2017-09-11 01:29:55'),(10,11,'\r3075-3080','-22,71952745','-49,44991235','2017-09-11 01:29:55','2017-09-11 01:29:55');
/*!40000 ALTER TABLE `local_carga_sub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `tabela` varchar(45) DEFAULT NULL,
  `id_objeto` int(11) DEFAULT NULL,
  `acao` varchar(15) DEFAULT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (2,8,'camada',8,'cadastrar','2017-09-11 03:50:03'),(3,8,'camada',8,'editar','2017-09-11 03:51:54'),(4,8,'camada',8,'excluir','2017-09-11 04:02:30'),(5,8,'camada',9,'cadastrar','2017-09-11 04:03:26'),(6,8,'camada',9,'excluir','2017-09-11 04:03:29');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidade` int(11) DEFAULT NULL,
  `densidade` int(11) DEFAULT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insercao_manual` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` VALUES (1,1,1,'Massa Asfaltica - CBUQ','2017-06-29 18:24:18','2017-08-27 05:31:34',0),(2,2,2,'Argila','2017-06-27 17:32:22','2017-08-27 05:28:32',0),(3,3,3,'Areia','2017-06-27 17:32:35','2017-08-27 05:28:32',0),(4,0,4,'teste','2017-08-07 05:42:16','2017-08-27 05:28:32',0),(5,0,5,'teste','2017-08-07 05:42:22','2017-08-27 05:28:32',0);
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material_projeto`
--

DROP TABLE IF EXISTS `material_projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material_projeto` (
  `id_material` int(11) NOT NULL,
  `id_projeto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material_projeto`
--

LOCK TABLES `material_projeto` WRITE;
/*!40000 ALTER TABLE `material_projeto` DISABLE KEYS */;
INSERT INTO `material_projeto` VALUES (2,1),(3,1),(1,1);
/*!40000 ALTER TABLE `material_projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacao`
--

DROP TABLE IF EXISTS `operacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_veiculo` int(11) DEFAULT NULL,
  `id_operador` int(11) DEFAULT NULL,
  `id_tipo_atividade` int(11) DEFAULT NULL,
  `id_origem` int(11) DEFAULT NULL,
  `id_destino` int(11) DEFAULT NULL,
  `id_origem_material` int(11) DEFAULT NULL,
  `id_camada` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `qtde_volume` int(11) DEFAULT NULL,
  `id_tipo_volume` int(11) DEFAULT NULL,
  `complemento_manutencao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacao`
--

LOCK TABLES `operacao` WRITE;
/*!40000 ALTER TABLE `operacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `operacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operador_origem`
--

DROP TABLE IF EXISTS `operador_origem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operador_origem` (
  `id_pessoa` int(11) DEFAULT NULL,
  `id_origem` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operador_origem`
--

LOCK TABLES `operador_origem` WRITE;
/*!40000 ALTER TABLE `operador_origem` DISABLE KEYS */;
INSERT INTO `operador_origem` VALUES (2,2);
/*!40000 ALTER TABLE `operador_origem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operador_veiculo`
--

DROP TABLE IF EXISTS `operador_veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operador_veiculo` (
  `id_pessoa` int(11) DEFAULT NULL,
  `id_veiculo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operador_veiculo`
--

LOCK TABLES `operador_veiculo` WRITE;
/*!40000 ALTER TABLE `operador_veiculo` DISABLE KEYS */;
INSERT INTO `operador_veiculo` VALUES (1,1),(1,2),(2,1),(8,20);
/*!40000 ALTER TABLE `operador_veiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origem_material`
--

DROP TABLE IF EXISTS `origem_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origem_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_origem` int(11) NOT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `modelo` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origem_material`
--

LOCK TABLES `origem_material` WRITE;
/*!40000 ALTER TABLE `origem_material` DISABLE KEYS */;
INSERT INTO `origem_material` VALUES (1,1,1,'UA-134','1','2017-06-29 18:09:35','2017-09-09 13:57:21'),(2,1,1,'ES-61','asd','2017-06-29 18:09:35','2017-09-09 13:57:25'),(3,0,1,'ES-69','dsa','2017-06-29 18:09:35','2017-09-09 13:57:28'),(6,1,2,'UA-13','a','2017-08-04 03:12:25','2017-09-09 13:57:36');
/*!40000 ALTER TABLE `origem_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_pessoa` int(11) DEFAULT NULL,
  `nome` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `senha` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `hash_esqueceu_senha` varchar(40) DEFAULT NULL,
  `esqueceu_senha_valido` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,1,4,'Guilherme Reis Fernandes','Rua teste, 1900','000.000.000-30','guilherme.fernandes','698dc19d489c4e4db73e28a713eab07b','guilherme@englevel.com.br','(41) 9999-9999',NULL,0,'2017-06-27 17:59:23','2017-08-06 22:39:26'),(2,1,2,'Jose Nogueira da Costa Junior','Rua Alphonse Daudet, 219','989.808.123-61','jose.nogueira','698dc19d489c4e4db73e28a713eab07b','jose.costa@actiotech.com.br','(41) 99147-0940',NULL,0,'2017-06-28 20:03:31','2017-08-07 01:47:54'),(8,1,2,'Julyano','Teste','091.866.199-42','teste','698dc19d489c4e4db73e28a713eab07b','julyano@jbrsolutions.com.br','(41) 9999-9999','72c1c5f9d215dd7cfd272987af3b4623',0,'2017-07-31 05:42:23','2017-09-11 03:31:12'),(9,0,2,'dasdsaasdsad','asdsadsa','055.678.530-41','asdasd','a8f5f167f44f4964e6c998dee827110c','dsad@asdsadsa','(33) 21321-4323',NULL,0,'2017-08-06 23:47:38','2017-08-27 15:46:13');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto`
--

DROP TABLE IF EXISTS `projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `nome` varchar(70) DEFAULT NULL,
  `lote` varchar(20) DEFAULT NULL,
  `extensao` varchar(20) DEFAULT NULL,
  `executor` varchar(80) DEFAULT NULL,
  `cidade` varchar(40) DEFAULT NULL,
  `Estado` varchar(30) DEFAULT NULL,
  `prazo_contratual` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto`
--

LOCK TABLES `projeto` WRITE;
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
INSERT INTO `projeto` VALUES (1,1,'Projeto Jaciara','1A','3000','Guilherme','Jaciara','MS','12 meses','2017-06-27 17:23:35','2017-07-31 02:01:51'),(2,1,'p2','das','dsa','dsa','dsad','DS','dsad','2017-08-11 00:59:09','2017-08-11 00:59:09');
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terceiros`
--

DROP TABLE IF EXISTS `terceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terceiros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terceiros`
--

LOCK TABLES `terceiros` WRITE;
/*!40000 ALTER TABLE `terceiros` DISABLE KEYS */;
INSERT INTO `terceiros` VALUES (3,'Terceiro','(41) 9630-9233','julykramer@hotmail.com');
/*!40000 ALTER TABLE `terceiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_atividade`
--

DROP TABLE IF EXISTS `tipo_atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_atividade`
--

LOCK TABLES `tipo_atividade` WRITE;
/*!40000 ALTER TABLE `tipo_atividade` DISABLE KEYS */;
INSERT INTO `tipo_atividade` VALUES (1,'Transporte de material','','2017-06-27 17:50:57','2017-08-05 17:03:05'),(2,'Transporte vazio',NULL,'2017-06-27 17:50:57','2017-06-27 17:50:57'),(3,'Manutencao',NULL,'2017-06-29 12:51:39','2017-06-27 17:51:19'),(4,'Limpeza',NULL,'2017-06-27 17:51:19','2017-06-27 17:51:19'),(5,'Chuva',NULL,'2017-06-27 17:51:29','2017-06-27 17:51:29'),(13,'Inatividade','','2017-08-07 00:14:20','2017-08-08 21:50:45');
/*!40000 ALTER TABLE `tipo_atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_camada`
--

DROP TABLE IF EXISTS `tipo_camada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_camada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_camada`
--

LOCK TABLES `tipo_camada` WRITE;
/*!40000 ALTER TABLE `tipo_camada` DISABLE KEYS */;
INSERT INTO `tipo_camada` VALUES (1,'Camada 1','2017-08-04 05:06:36','2017-08-04 05:06:36'),(2,'Camada 2','2017-08-04 05:06:36','2017-08-04 05:06:36');
/*!40000 ALTER TABLE `tipo_camada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_inatividade`
--

DROP TABLE IF EXISTS `tipo_inatividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_inatividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_atividade` int(11) DEFAULT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_inatividade`
--

LOCK TABLES `tipo_inatividade` WRITE;
/*!40000 ALTER TABLE `tipo_inatividade` DISABLE KEYS */;
INSERT INTO `tipo_inatividade` VALUES (10,13,'dasdsasda','','2017-08-07 00:14:31','2017-08-07 03:42:13');
/*!40000 ALTER TABLE `tipo_inatividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_local`
--

DROP TABLE IF EXISTS `tipo_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_local`
--

LOCK TABLES `tipo_local` WRITE;
/*!40000 ALTER TABLE `tipo_local` DISABLE KEYS */;
INSERT INTO `tipo_local` VALUES (1,'Carregamento','2017-06-27 17:38:26','2017-06-27 17:38:26'),(2,'Descarregamento','2017-06-27 17:38:26','2017-08-04 04:54:33');
/*!40000 ALTER TABLE `tipo_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_manutencao`
--

DROP TABLE IF EXISTS `tipo_manutencao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_manutencao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_manutencao`
--

LOCK TABLES `tipo_manutencao` WRITE;
/*!40000 ALTER TABLE `tipo_manutencao` DISABLE KEYS */;
INSERT INTO `tipo_manutencao` VALUES (1,'Troca de pastilha de freio',NULL,'2017-06-27 17:49:27','2017-06-27 17:49:27'),(2,'Troca de oleo',NULL,'2017-06-30 18:17:49','2017-06-27 17:49:27'),(3,'Manutencao programada',NULL,'2017-06-30 18:17:58','2017-06-27 17:49:55'),(4,'Manutencao imprevista',NULL,'2017-06-30 18:22:31','2017-06-27 17:49:55'),(5,'Outros',NULL,'2017-06-27 17:50:06','2017-06-27 17:50:06');
/*!40000 ALTER TABLE `tipo_manutencao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_origem`
--

DROP TABLE IF EXISTS `tipo_origem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_origem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_origem`
--

LOCK TABLES `tipo_origem` WRITE;
/*!40000 ALTER TABLE `tipo_origem` DISABLE KEYS */;
INSERT INTO `tipo_origem` VALUES (1,'Tipo Origem 1','2017-08-04 02:51:22','2017-08-04 02:51:22'),(2,'Tipo Origem 2','2017-08-04 02:51:22','2017-08-04 02:51:22');
/*!40000 ALTER TABLE `tipo_origem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pessoa`
--

DROP TABLE IF EXISTS `tipo_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `acesso` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pessoa`
--

LOCK TABLES `tipo_pessoa` WRITE;
/*!40000 ALTER TABLE `tipo_pessoa` DISABLE KEYS */;
INSERT INTO `tipo_pessoa` VALUES (1,'Motorista',1,'2017-06-27 17:54:45','2017-08-04 07:25:22'),(2,'Admin. Geral',3,'2017-06-27 17:54:45','2017-08-04 06:44:20'),(3,'Operador',1,'2017-06-27 17:55:00','2017-08-04 06:49:09'),(4,'Admin. Obra',3,'2017-06-27 17:55:00','2017-08-04 06:40:40'),(55,'Gestor de Contrato',3,'2017-08-27 14:26:59','2017-08-27 14:26:59');
/*!40000 ALTER TABLE `tipo_pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_veiculo`
--

DROP TABLE IF EXISTS `tipo_veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_veiculo`
--

LOCK TABLES `tipo_veiculo` WRITE;
/*!40000 ALTER TABLE `tipo_veiculo` DISABLE KEYS */;
INSERT INTO `tipo_veiculo` VALUES (1,'Caminhao','2017-06-28 20:03:10','2017-06-27 19:11:55'),(2,'Caminhao fora de estrada','2017-06-28 20:03:15','2017-06-27 19:11:55'),(3,'Maquina','2017-06-28 20:03:21','2017-06-27 19:11:55');
/*!40000 ALTER TABLE `tipo_veiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade`
--

LOCK TABLES `unidade` WRITE;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` VALUES (1,'M²','2017-06-27 17:28:42','2017-06-27 17:28:42'),(2,'Ton','2017-06-27 17:29:06','2017-06-27 17:29:06'),(3,'Kg','2017-06-27 17:29:06','2017-06-27 17:29:06');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculo`
--

DROP TABLE IF EXISTS `veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_veiculo` int(11) DEFAULT NULL,
  `id_terceiro` int(11) DEFAULT '0',
  `codigo` varchar(20) DEFAULT NULL,
  `placa` varchar(20) DEFAULT NULL,
  `marca` varchar(30) DEFAULT NULL,
  `modelo` varchar(30) DEFAULT NULL,
  `implemento` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `capacidade` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculo`
--

LOCK TABLES `veiculo` WRITE;
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` VALUES (1,1,1,0,'CB-001','GGG-0000','VOLVO','VOLVO',NULL,'2017-06-27 19:24:53','2017-09-08 20:13:55',0),(2,1,1,0,'CB-002','HHH-1111','SCANIA','SCANIA',NULL,'2017-06-27 19:24:53','2017-09-08 20:13:55',0),(20,1,2,0,'sdadasd','DSA4555','adasdsadsa','dsadsadsasdadsa',NULL,'2017-08-06 23:25:58','2017-09-08 20:13:55',76575),(21,2,3,0,'dsadsadsadsa','DSA-9989','dsadsadsa','sdadsadsa',NULL,'2017-08-07 01:31:08','2017-09-08 20:13:55',2),(22,0,1,0,'41114','ASD-1113','asd','asd',NULL,'2017-09-09 13:35:20','2017-09-09 13:38:15',13);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-11  1:05:40
