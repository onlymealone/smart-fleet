<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
  include('../phpqrcode/qrlib.php');
  define('IMAGE_WIDTH', 550);
  define('IMAGE_HEIGHT', 550);

  $codigo = $obj->qrcode;
  $tempFile = md5(date('Y-m-d H:i:s' . substr((string)microtime(), 1, 8))) . '.png';
  $tempModelo = 'tempModelo.png';
  QRCode::png($codigo, $tempFile, QR_ECLEVEL_L, 14, 1);
  $qrcode = imagecreatefrompng($tempFile);
  unlink($tempFile);
  $imagem = imagecreatefrompng('../../img/base_cracha.png');

  $textoPreto = imagecolorallocate($imagem, 0, 0, 0);
  $caminhoFonte = '../../fonts/arial.ttf';
  $nome = $obj->usuario->nome;
  $cpf = $obj->usuario->cpf;
  $funcao = $obj->usuario->tipo;

  imagettftext($imagem, 25, 0, 50, 690, $textoPreto, $caminhoFonte, $nome);
  imagettftext($imagem, 25, 0, 50, 820, $textoPreto, $caminhoFonte, $cpf);
  imagettftext($imagem, 25, 0, 50, 945, $textoPreto, $caminhoFonte, $funcao);
  imagecopymerge($imagem, $qrcode, 790, 120, 0, 0, 550, 550, 100);

  imagepng($imagem, $tempModelo);

  $imgbinary = fread(fopen($tempModelo, "r"), filesize($tempModelo));
  $img_str = base64_encode($imgbinary);

  unlink($tempModelo);
  $retorno = array();
  $retorno['imagem'] = $img_str;
  echo json_encode($retorno);
}
