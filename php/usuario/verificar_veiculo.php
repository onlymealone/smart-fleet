<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	$query = "DELETE FROM operador_veiculo WHERE id_pessoa = $obj->operador";
	mysqli_query($con,$query);
	foreach($obj->veiculo as $veiculo){
		$query = "INSERT INTO operador_veiculo VALUES($obj->operador, $veiculo)";
		mysqli_query($con,$query);
	}
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $obj->usuario_sessao, "vinculou/desvinculou os veículos do usuário $obj->operador.");
	}

	echo json_encode($retorno);
}
