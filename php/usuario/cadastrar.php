<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	function verificarUsuario($usuario, $con, &$retorno){
		$query = "SELECT * FROM pessoa where login = '" . $usuario->login . "'";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['status'] = 2;
			return false;
		}
		$query = "SELECT * FROM pessoa where cpf = '" . $usuario->cpf . "'";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['status'] = 3;
			return false;
		}
		return true;
	}

	$usuario = $dados;

	$retorno['status'] = 1;

	if(verificarUsuario($usuario, $con, $retorno)) {
		$query = "INSERT INTO pessoa
				(id_projeto, id_tipo_pessoa, nome, endereco, cpf, login, senha, email, telefone)
				VALUES ('" .
				$usuario->id_projeto . "', '" .
				$usuario->id_tipo_pessoa . "', '" .
				$usuario->nome . "', '" .
				$usuario->endereco . "', '" .
				$usuario->cpf . "', '" .
				$usuario->login . "', '" .
				md5($usuario->senha) . "', '" .
				$usuario->email ."', '" .
				$usuario->telefone . "');";
		mysqli_query($con,$query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			$id =  mysqli_insert_id($con);
			gerarLog($con, $usuario->usuario_sessao, "cadastrou o usuário $id.");
		}
	}

	echo json_encode($retorno);

}
