<?php

require_once('../conexao.php');

$query = "SELECT id, descricao FROM tipo_pessoa WHERE excluido = FALSE";
$qryLista = mysqli_query($con, $query);
$retorno['status'] = 1;
$retorno['tipos'] = array();

if(mysqli_num_rows($qryLista) == 0){
	$retorno['status'] = 0;
}

while($resultado = mysqli_fetch_assoc($qryLista)){
	array_push($retorno['tipos'], $resultado);
}
echo json_encode($retorno);
