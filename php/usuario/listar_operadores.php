<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');

	$query = "SELECT p.id, p.nome, pj.nome as nome_projeto FROM pessoa p INNER JOIN projeto pj ON pj.id = p.id_projeto INNER JOIN empresa e ON e.id = pj.id_empresa WHERE p.excluido = FALSE AND pj.id = $obj->projeto";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['usuarios'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		$query = "SELECT ov.*, v.codigo, v.id as id_veiculo FROM operador_veiculo ov INNER JOIN veiculo v ON v.id = ov.id_veiculo WHERE id_pessoa = " . $resultado['id'];
		$qryLista2 = mysqli_query($con, $query);
		if(mysqli_num_rows($qryLista2) > 0){
			while($resultado2 = mysqli_fetch_assoc($qryLista2)){
				if($resultado2['id_pessoa'] == $resultado['id']){
					if(!isset($resultado['vinculo'])){
						$resultado['vinculo'] = array();
						$resultado['veiculos'] = array();
					}
					array_push($resultado['vinculo'], $resultado2['codigo']);
					array_push($resultado['veiculos'], $resultado2['id_veiculo']);
				}
			}
		}
		if(!isset($resultado['vinculo'])){
			$resultado['vinculo'] = 'Nenhum';
		}elseif(count($resultado['vinculo']) == 1){
			$resultado['vinculo'] = $resultado['vinculo'][0];
		}
		array_push($retorno['usuarios'], $resultado);
	}
	echo json_encode($retorno);
}
