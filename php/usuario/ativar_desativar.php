<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');

	$query = "UPDATE usuarios SET status = 1 WHERE id =" . $obj->id;
	$msg = "ativou o usuario $obj->id.";
	if($obj->status){
		$query = "UPDATE usuarios SET status = 0 WHERE id =" . $obj->id;
		$msg = "desativou o usuario $obj->id.";
	}
	mysqli_query($con, $query);
	if(!$con->error){
		gerarLog($con, $obj->usuario_sessao, $msg);
	}
	$retorno = array();
	$retorno['status'] = 1;
	echo json_encode($retorno);
}
