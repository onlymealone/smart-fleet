<?php
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	require_once('../conexao.php');

	$query = "SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'pessoa' AND table_schema = DATABASE( ) ;";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}else{
		$obj = mysqli_fetch_assoc($qryLista);
		$retorno['id'] = $obj['AUTO_INCREMENT'];
	}
	echo json_encode($retorno);
}
