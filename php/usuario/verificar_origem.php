<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	$query = "DELETE FROM operador_origem WHERE id_pessoa = $obj->operador";
	mysqli_query($con,$query);
	foreach($obj->origem as $origem){
		$query = "INSERT INTO operador_origem VALUES($obj->operador, $origem)";
		mysqli_query($con,$query);
	}
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $obj->usuario_sessao, "vinculou/desvinculou as origens do usuário $obj->operador.");
	}

	echo json_encode($retorno);
}
