<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');

	$query = "SELECT m.*, u.descricao as unidade FROM material m INNER JOIN unidade u ON u.id = m.id_unidade WHERE m.excluido = FALSE;";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['materiais'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		$resultado['id_projeto'] = 0;
		$query2 = "SELECT * FROM material_projeto WHERE id_material = " . $resultado['id'] . " AND id_projeto = " . $obj->id_projeto;
		$qryLista2 = mysqli_query($con, $query2);
		if(mysqli_num_rows($qryLista2) > 0){
			$obj2 = mysqli_fetch_assoc($qryLista2);
			$resultado['id_projeto'] = $obj2['id_projeto'];
		}
		array_push($retorno['materiais'], $resultado);
	}
	echo json_encode($retorno);
}
