<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	$id = $obj->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	$query = "SELECT * FROM material_projeto WHERE id_material = " . $id;
	$qryLista = mysqli_query($con, $query);
	if(mysqli_num_rows($qryLista) > 0){
		$retorno['status'] = 0;
	}else{
		$query = "UPDATE material SET excluido = TRUE WHERE id =" . $id;
		mysqli_query($con, $query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			gerarLog($con, $obj->usuario_sessao, "excluiu o material $id.");
		}
	}
	echo json_encode($retorno);
}
