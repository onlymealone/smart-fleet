<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	$material = $dados;

	$retorno['status'] = 1;
	if(!$material->complemento_manual){
		$material->complemento_manual = 0;
	}else{
		$material->complemento_manual = 1;
	}
	$query = "UPDATE material
			SET
			id_unidade = '" . $material->id_unidade . "',
			descricao = '" . $material->descricao . "',
			densidade = '" . $material->densidade . "',
			insercao_manual = '" . $material->complemento_manual . "'
		 	WHERE id = " . $material->id;
	if(!mysqli_query($con, $query)){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $material->usuario_sessao, "editou o material $id.");
	}

	echo json_encode($retorno);

}
