<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	function verificarVeiculo($veiculo, $con, &$retorno){
		$query = "SELECT * FROM veiculo WHERE id != " . $veiculo->id . " AND codigo = '" . $veiculo->codigo . "'";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['status'] = 2;
			return false;
		}else{
			$query = "SELECT * FROM veiculo WHERE id != " . $veiculo->id . " AND placa = '" . $veiculo->placa . "'";
			$qryLista = mysqli_query($con,$query);
			if(mysqli_num_rows($qryLista) > 0){
				$retorno['status'] = 3;
				return false;
			}
		}
		return true;
	}

	$veiculo = $dados;

	$retorno['status'] = 1;

	if(verificarVeiculo($veiculo, $con, $retorno)) {
		$query = "UPDATE veiculo
				SET
				id_tipo_veiculo = '" . $veiculo->id_tipo_veiculo . "',
				id_terceiro = '" . $veiculo->id_terceiro . "',
				codigo = '" . $veiculo->codigo . "',
				placa = '" .	$veiculo->placa . "',
				marca = '" .	$veiculo->marca . "',
				capacidade = '" .	$veiculo->capacidade . "',
				modelo = '" .	$veiculo->modelo . "'
				 WHERE id = " . $veiculo->id;
		if(!mysqli_query($con, $query)){
			$retorno['status'] = 0;
		}else{
			gerarLog($con, $veiculo->usuario_sessao, "editou o veículo $veiculo->id.");
		}
	}

	echo json_encode($retorno);

}
