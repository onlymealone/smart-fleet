<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
  include('../phpqrcode/qrlib.php');
  define('IMAGE_WIDTH', 300);
  define('IMAGE_HEIGHT', 300);

  $final = imagecreatetruecolor(IMAGE_WIDTH, IMAGE_HEIGHT+75);
  imagealphablending($final, false);
  imagesavealpha($final, true);
  $trans_background = imagecolorallocatealpha($final, 255, 255, 255, 127);
  imagefill($final, 0, 0, $trans_background);

  $codigo = $obj->qrcode;
  $tempFile = md5(date('Y-m-d H:i:s' . substr((string)microtime(), 1, 8))) . '.png';
  QRCode::png($codigo, $tempFile);
  $qr = imagecreatefrompng($tempFile);
  unlink($tempFile);

  $logo = imagecreatefrompng('../../img/logo-completa-transparente.png');
  $srcWidth = imagesx($logo);
  $srcHeight = imagesy($logo);
  $nWidth = intval(1200 / 4);
  $nHeight = intval(300 /4);
  $newImg = imagecreatetruecolor($nWidth, $nHeight);
  imagealphablending($newImg, false);
  imagesavealpha($newImg,true);
  $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
  imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
  imagecopyresampled($newImg, $logo, 0, 0, 0, 0, $nWidth, $nHeight,
      $srcWidth, $srcHeight);

  imagecopy($final, $newImg, 3, 0, 0, 0, '297', '300');
  imagecopy($final, $qr, 0, 75, 0, 0, IMAGE_WIDTH+10, IMAGE_HEIGHT+10);
  imagecopymerge($logo, $qr, 0, 0, 0, 0, 1280, 1280, 100);

  imagepng($final, $tempFile);
  $imgbinary = fread(fopen($tempFile, "r"), filesize($tempFile));
  $img_str = base64_encode($imgbinary);
  unlink($tempFile);


  $retorno = array();
  $retorno['imagem'] = $img_str;
  echo json_encode($retorno);
}
