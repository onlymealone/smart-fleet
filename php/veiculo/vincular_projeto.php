<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	$query = "UPDATE veiculo SET id_projeto = $obj->id_projeto WHERE id = $obj->id_veiculo";
	mysqli_query($con, $query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $obj->usuario_sessao, "vinculou o veículo $obj->id_veiculo ao projeto $obj->id_projeto.");
	}

	echo json_encode($retorno);
}
