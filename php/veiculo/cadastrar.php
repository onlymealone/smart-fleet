<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	function verificarVeiculo($veiculo, $con, &$retorno){
		$query = "SELECT * FROM veiculo where codigo = '" . $veiculo->codigo . "'";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['status'] = 2;
			return false;
		}else{
			$query = "SELECT * FROM veiculo where placa = '" . $veiculo->placa . "'";
			$qryLista = mysqli_query($con,$query);
			if(mysqli_num_rows($qryLista) > 0){
				$retorno['status'] = 3;
				return false;
			}
		}
		return true;
	}

	$veiculo = $dados;

	$retorno['status'] = 1;

	if(verificarVeiculo($veiculo, $con, $retorno)) {
		$query = "INSERT INTO veiculo
				(id_projeto, id_tipo_veiculo, id_terceiro, codigo, placa, marca, capacidade, modelo)
				VALUES ('" .
				$veiculo->id_projeto . "', '" .
				$veiculo->id_tipo_veiculo . "', '" .
				$veiculo->id_terceiro . "', '" .
				$veiculo->codigo . "', '" .
				$veiculo->placa . "', '" .
				$veiculo->marca . "', '" .
				$veiculo->capacidade . "', '" .
				$veiculo->modelo . "'" .
				");";
		mysqli_query($con,$query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			$id =  mysqli_insert_id($con);
			gerarLog($con, $veiculo->usuario_sessao, "cadastrou o veículo $id.");
		}
	}

	echo json_encode($retorno);

}
