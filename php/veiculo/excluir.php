<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($id)){
	$id = $dados->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	$query = "SELECT * FROM operador_veiculo WHERE id_veiculo = " . $id;
	$qryLista = mysqli_query($con, $query);
	if(mysqli_num_rows($qryLista) > 0){
		$retorno['status'] = 0;
	}else{
		$query = "UPDATE veiculo SET excluido = TRUE WHERE id =" . $id;
		mysqli_query($con, $query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			gerarLog($con, $dados->usuario_sessao, "excluiu o veículo $id.");
		}
	}
	echo json_encode($retorno);
}
