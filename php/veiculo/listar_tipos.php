<?php

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	require_once('../conexao.php');

	$query = "SELECT * FROM tipo_veiculo WHERE excluido = FALSE;";
	$qryLista = mysqli_query($con, $query);
	$retorno = array();
	$retorno['status'] = 1;
	$retorno['tipo_veiculo'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['tipo_veiculo'], $resultado);
	}
	echo json_encode($retorno);
}
