<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');

	$query = "SELECT v.id, v.codigo, v.placa, pj.nome as nome_projeto FROM veiculo v INNER JOIN projeto pj ON pj.id = v.id_projeto WHERE v.excluido = FALSE AND pj.id = $obj->projeto";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['veiculos'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		$query = "SELECT ov.*, p.nome, p.id as id_pessoa FROM operador_veiculo ov INNER JOIN pessoa p ON p.id = ov.id_pessoa WHERE id_veiculo = " . $resultado['id'];
		$qryLista2 = mysqli_query($con, $query);
		if(mysqli_num_rows($qryLista2) > 0){
			while($resultado2 = mysqli_fetch_assoc($qryLista2)){
				if($resultado2['id_veiculo'] == $resultado['id']){
					if(!isset($resultado['vinculo'])){
						$resultado['vinculo'] = array();
					}
					array_push($resultado['vinculo'], $resultado2['nome']);
					$resultado['id_vinculo'] = $resultado2['id_pessoa'];
				}
			}
		}
		if(!isset($resultado['vinculo'])){
			$resultado['vinculo'] = 'Nenhum';
		}elseif(count($resultado['vinculo']) == 1){
			$resultado['vinculo'] = $resultado['vinculo'][0];
		}
		array_push($retorno['veiculos'], $resultado);
	}
	echo json_encode($retorno);
}
