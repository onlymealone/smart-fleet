<?php
$param = json_decode(file_get_contents('php://input'));
if(isset($param)){
	require_once('../conexao.php');

	$query = "SELECT l.*, DATE_FORMAT(l.data, '%d/%m/%Y %H:%i:%s') as data_formatada, p.nome FROM log l INNER JOIN pessoa p ON p.id = l.id_usuario ORDER BY l.id DESC";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['logs'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['logs'], $resultado);
	}
	echo json_encode($retorno);
}
