<?php
if($_SERVER['REQUEST_METHOD'] === 'POST'){
	require_once('../conexao.php');

	$query = "SELECT * FROM terceiros WHERE excluido = FALSE;";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['terceiros'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['terceiros'], $resultado);
	}
	echo json_encode($retorno);
}
