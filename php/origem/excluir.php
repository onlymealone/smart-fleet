<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($id)){
	$id = $dados->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	$query = "UPDATE origem_material SET excluido = TRUE WHERE id =" . $id;
	mysqli_query($con, $query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $dados->usuario_sessao, "excluiu a origem $id.");
	}
	echo json_encode($retorno);
}
