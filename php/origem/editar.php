<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	$origem = $dados;

	$retorno['status'] = 1;

	$query = "UPDATE origem_material
			SET
			id_tipo_origem = '" . $origem->id_tipo_origem . "',
			descricao = '" . $origem->descricao . "',
			modelo = '" . $origem->modelo . "'
		 	WHERE id = " . $origem->id;
	if(!mysqli_query($con, $query)){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $origem->usuario_sessao, "editou a origem $origem->id.");
	}

	echo json_encode($retorno);

}
