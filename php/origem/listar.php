<?php
// $id = json_decode(file_get_contents('php://input'));
// if(isset($id)){
	require_once('../conexao.php');

	$query = "SELECT o.*, tio.descricao as tipo_origem FROM origem_material o INNER JOIN tipo_origem tio ON tio.id = o.id_tipo_origem WHERE o.excluido = FALSE";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['origens'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		$query = "SELECT p.nome as nome_projeto FROM projeto p WHERE id = " . $resultado['id_projeto'];
		$qryLista2 = mysqli_query($con, $query);
		$resultado['vinculo'] = 'Nenhum';
		if(mysqli_num_rows($qryLista2) > 0){
			$qryLista2 = mysqli_fetch_assoc($qryLista2);
			$resultado['vinculo'] = $qryLista2['nome_projeto'];
		}
		array_push($retorno['origens'], $resultado);
	}
	echo json_encode($retorno);
// }
