<?php
$id = json_decode(file_get_contents('php://input'));
if(isset($id)){
	require_once('../conexao.php');

	$query = "SELECT l.id, l.id_projeto, l.id_tipo_local, l.latitude, l.longitude, p.nome as nome_projeto, tl.descricao as nome_tipo, l.complemento_manual, l.descricao FROM local_carga l INNER JOIN projeto p ON p.id = l.id_projeto INNER JOIN tipo_local tl ON tl.id = l.id_tipo_local WHERE p.id = $id AND l.excluido = FALSE";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['locais'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['locais'], $resultado);
	}
	echo json_encode($retorno);
}
