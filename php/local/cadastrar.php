<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	$local = $dados;
	if(!isset($local->latitude)){
		$local->latitude = "";
	}
	if(!isset($local->longitude)){
		$local->longitude = "";
	}
	$retorno['status'] = 1;

	if(!isset($local->complemento_manual)){
		$local->complemento_manual = 0;
	}

	$query = "INSERT INTO local_carga
			(id_projeto, id_tipo_local, descricao, latitude, longitude, complemento_manual)
			VALUES ('" .
			$local->id_projeto . "', '" .
			$local->id_tipo_local . "', '" .
			$local->descricao . "', '" .
			$local->latitude . "', '" .
			$local->longitude . "', '" .
			$local->complemento_manual . "'" .
			");";
	mysqli_query($con,$query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		$id =  mysqli_insert_id($con);
		gerarLog($con, $local->usuario_sessao, "cadastrou o local $id.");
	}

	// if(!isset($local->complemento_manual)){
	// 	$local->complemento_manual = 'false';
	// }else{
	// 	$local->complemento_manual = 'true';
	// }

	// $url = 'http://feroxhome.mooo.com:8080/smartfleet-web/api/v1/localcarga';
	// $body = '{
	//     "complementoManual": ' . $local->complemento_manual . ',
	//     "descricao": "' . $local->descricao . '",
	//     "latitude": ' . $local->latitude . ',
	//     "longitude": ' . $local->longitude . ',
	//     "projeto": {"id": ' . $local->id_projeto . '}
	// }';
	// $options = array(
	//         'http' => array(
	//         'header'  => "Content-type: application/json\r\n",
	//         'method'  => 'POST',
	//         'content' => $body,
	//     )
	// );
	//
	// $context  = stream_context_create($options);
	// $result = file_get_contents($url, false, $context);
	// $obj = json_decode($result, true);
	//
	// gerarLog($con, $local->usuario_sessao, "cadastrou o local " . $obj["data"]["id"] . ".");
	echo json_encode($retorno);

}
