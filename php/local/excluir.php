<?php
$local = json_decode(file_get_contents('php://input'));
if(isset($local)){
	$id = $local->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	$query = "UPDATE local_carga SET excluido = TRUE WHERE id =" . $id;
	mysqli_query($con, $query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $local->usuario_sessao, "excluiu o local $id.");
	}

	// $url = 'http://feroxhome.mooo.com:8080/smartfleet-web/api/v1/localcarga/' . $id;
	// $options = array(
	//         'http' => array(
	//         'header'  => "Content-type: application/json\r\n",
	//         'method'  => 'DELETE',
	//     )
	// );
	//
	// $context  = stream_context_create($options);
	// $result = file_get_contents($url, false, $context);
	// $obj = json_decode($result, true);
	// gerarLog($con, $local->usuario_sessao, "excluiu o local $id.");

	echo json_encode($retorno);
}
