<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	$local = $dados;
	if(!isset($local->latitude)){
		$local->latitude = "";
	}
	if(!isset($local->longitude)){
		$local->longitude = "";
	}
	$retorno['status'] = 1;

	if(!isset($local->complemento_manual)){
		$local->complemento_manual = 0;
	}
	$query = "UPDATE local_carga
			SET
			id_tipo_local = '" . $local->id_tipo_local . "',
			descricao = '" . $local->descricao . "',
			latitude = '" .	$local->latitude . "',
			longitude = '" .	$local->longitude . "',
			complemento_manual = '" .	$local->complemento_manual . "'
		 	WHERE id = " . $local->id;
	if(!mysqli_query($con, $query)){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $local->usuario_sessao, "editou o local $local->id.");
	}

	// if(!isset($local->complemento_manual)){
	// 	$local->complemento_manual = 'false';
	// }else{
	// 	$local->complemento_manual = 'true';
	// }
	//
	// $url = 'http://feroxhome.mooo.com:8080/smartfleet-web/api/v1/localcarga/' . $local->id;
	// $body = '{
	//     "complementoManual": ' . $local->complemento_manual . ',
	//     "descricao": "' . $local->descricao . '",
	//     "latitude": ' . $local->latitude . ',
	//     "longitude": ' . $local->longitude . ',
	//     "projeto": {"id": ' . $local->id_projeto . '}
	// }';
	// $options = array(
	//         'http' => array(
	//         'header'  => "Content-type: application/json\r\n",
	//         'method'  => 'PUT',
	//         'content' => $body,
	//     )
	// );
	//
	// $context  = stream_context_create($options);
	// $result = file_get_contents($url, false, $context);
	// $obj = json_decode($result, true);
	//
	// gerarLog($con, $local->usuario_sessao, "editou o local $local->id.");

	echo json_encode($retorno);

}
