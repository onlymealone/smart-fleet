<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	if($obj->tipo == 'tipo_atividade'){
		$query = "SELECT * FROM tipo_inatividade WHERE id_tipo_atividade = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'tipo_camada'){
		$query = "SELECT * FROM camada WHERE id_tipo_camada = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'tipo_local'){
		$query = "SELECT * FROM local_carga WHERE id_tipo_local = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'tipo_origem'){
		$query = "SELECT * FROM origem_material WHERE id_tipo_origem = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'tipo_pessoa'){
		$query = "SELECT * FROM pessoa WHERE id_tipo_pessoa = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'tipo_veiculo'){
		$query = "SELECT * FROM veiculo WHERE id_tipo_veiculo = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}elseif($obj->tipo == 'unidade'){
		$query = "SELECT * FROM material WHERE id_unidade = $obj->id";
		$qryLista = mysqli_query($con, $query);
	}
	if($con->error){
		$retorno['status'] = 0;
	}else{
		if(isset($qryLista)){
			if(mysqli_num_rows($qryLista) > 0){
				$retorno['status'] = 0;
			}else{
				$query = "UPDATE $obj->tipo SET excluido = TRUE WHERE id =" . $obj->id;
				mysqli_query($con, $query);

				if($con->error){
					$retorno['status'] = 0;
				}else{
					gerarLog($con, $obj->usuario_sessao, "editou o $obj->tipo $obj->id.");
				}
			}
		}else{
			$query = "DELETE FROM $obj->tipo WHERE id =" . $obj->id;
			mysqli_query($con, $query);
			if($con->error){
				$retorno['status'] = 0;
			}
		}
	}
	echo json_encode($retorno);
}
