<?php
$tipo = json_decode(file_get_contents('php://input'));
if(isset($tipo)){
	require_once('../conexao.php');
	$query = "SELECT * FROM $tipo->tipo WHERE excluido = FALSE";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['tipos'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['tipos'], $resultado);
	}
	echo json_encode($retorno);
}
