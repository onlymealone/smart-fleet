<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	if(!isset($obj->tipo->complemento)){
		$obj->tipo->complemento = "0";
	}
	if($obj->tipo_atual == 'tipo_inatividade'){
		$query = "SELECT id FROM tipo_atividade WHERE descricao = 'Inatividade' LIMIT 1;";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$query = "INSERT INTO $obj->tipo_atual
					(id_tipo_atividade, descricao, complemento)
					VALUES (
					(SELECT id FROM tipo_atividade WHERE descricao = 'Inatividade' LIMIT 1), '" .
					$obj->tipo->descricao . "', '" .
					$obj->tipo->complemento . "'" .
					");";
		}else{
			$retorno['status'] = 0;
		}
	}elseif($obj->tipo_atual == 'tipo_pessoa'){
		$query = "INSERT INTO $obj->tipo_atual
				(acesso, descricao)
				VALUES ('" .
				$obj->tipo->acesso . "', '" .
				$obj->tipo->descricao . "'" .
				");";
	}elseif($obj->tipo_atual == 'terceiros'){
		$query = "INSERT INTO $obj->tipo_atual
				(nome, telefone, email)
				VALUES ('" .
				$obj->tipo->nome . "', '" .
				$obj->tipo->telefone . "', '" .
				$obj->tipo->email . "'" .
				");";
	}else{
		if($obj->tipo_atual == 'tipo_atividade' || $obj->tipo_atual == 'tipo_manutencao'){
			$query = "INSERT INTO $obj->tipo_atual
					(descricao, complemento)
					VALUES ('" .
					$obj->tipo->descricao . "', '" .
					$obj->tipo->complemento . "'" .
					");";
		}else{
			$query = "INSERT INTO $obj->tipo_atual
					(descricao)
					VALUES ('" .
					$obj->tipo->descricao . "'" .
					");";
			}
	}
	if($retorno['status'] == 1){
		mysqli_query($con,$query);
		if(!$con->error){
			$id =  mysqli_insert_id($con);
			gerarLog($con, $obj->usuario_sessao, "cadastrou o $obj->tipo_atual $id.");
		}
	}
	if($con->error){
		$retorno['status'] = 0;
	}
	echo json_encode($retorno);

}
