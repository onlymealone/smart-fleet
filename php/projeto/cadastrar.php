<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($dados)){
	require_once('../conexao.php');

	function verificarProjeto($projeto, $con, &$retorno){
		$query = "SELECT * FROM projeto WHERE nome = '" . $projeto->nome . "'";
		$qryLista = mysqli_query($con,$query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['status'] = 2;
			return false;
		}
		return true;
	}

	$projeto = $dados;

	$retorno['status'] = 1;

	if(verificarProjeto($projeto, $con, $retorno)) {
		$query = "INSERT INTO projeto
				(nome, lote, extensao, executor, cidade, Estado, prazo_contratual, id_empresa)
				VALUES ('" . $projeto->nome . "', '" .
				$projeto->lote . "', '" .
				$projeto->extensao . "', '" .
				$projeto->executor . "', '" .
				$projeto->cidade . "', '" .
				$projeto->Estado . "', '" .
				$projeto->prazo_contratual . "', " .
				"(SELECT e.id FROM pessoa p INNER JOIN projeto pr ON pr.id = p.id_projeto INNER JOIN empresa e ON e.id = pr.id_empresa WHERE p.id =" . $projeto->id_pessoa . "));";
		mysqli_query($con,$query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			$id =  mysqli_insert_id($con);
			gerarLog($con, $projeto->id_pessoa, "cadastrou o projeto $id.");
		}
	}

	echo json_encode($retorno);

}
