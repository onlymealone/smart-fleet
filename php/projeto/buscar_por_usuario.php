<?php
$id = json_decode(file_get_contents('php://input'));
if(isset($id)){
	require_once('../conexao.php');

	$query = "SELECT pr.* FROM projeto pr WHERE pr.excluido = FALSE;";
	$qryLista = mysqli_query($con, $query);
	$retorno = array();
	$retorno['status'] = 1;
	$retorno['projetos'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['projetos'], $resultado);
	}
	echo json_encode($retorno);
}
