<?php
$dados = json_decode(file_get_contents('php://input'));
if(isset($id)){
	$id = $dados->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 0;
	$query = "SELECT * FROM operacao WHERE id_projeto =" . $id;
	$qryLista = mysqli_query($con, $query);
	if(mysqli_num_rows($qryLista) > 0){
		$retorno['erro'] = 'Esse projeto possui uma ou mais operações vinculadas!';
	}else{
		$query = "SELECT * FROM atividade WHERE id_projeto =" . $id;
		$qryLista = mysqli_query($con, $query);
		if(mysqli_num_rows($qryLista) > 0){
			$retorno['erro'] = 'Esse projeto possui uma ou mais atividades vinculadas!';
		}else{
			$query = "SELECT * FROM camada_projeto WHERE id_projeto =" . $id;
			$qryLista = mysqli_query($con, $query);
			if(mysqli_num_rows($qryLista) > 0){
				$retorno['erro'] = 'Esse projeto possui uma ou mais camadas vinculadas!';
			}else{
				$query = "SELECT * FROM local_carga WHERE id_projeto =" . $id;
				$qryLista = mysqli_query($con, $query);
				if(mysqli_num_rows($qryLista) > 0){
					$retorno['erro'] = 'Esse projeto possui um ou mais locais vinculados!';
				}else{
					$query = "SELECT * FROM material_projeto WHERE id_projeto =" . $id;
					$qryLista = mysqli_query($con, $query);
					if(mysqli_num_rows($qryLista) > 0){
						$retorno['erro'] = 'Esse projeto possui um ou mais materiais vinculados!';
					}else{
						$query = "SELECT * FROM origem_material WHERE id_projeto =" . $id;
						$qryLista = mysqli_query($con, $query);
						if(mysqli_num_rows($qryLista) > 0){
							$retorno['erro'] = 'Esse projeto possui uma ou mais origens vinculados!';
						}else{
							$query = "SELECT * FROM pessoa WHERE id_projeto =" . $id;
							$qryLista = mysqli_query($con, $query);
							if(mysqli_num_rows($qryLista) > 0){
								$retorno['erro'] = 'Esse projeto possui um ou mais usuários vinculados!';
							}else{
								$query = "SELECT * FROM veiculo WHERE id_projeto =" . $id;
								$qryLista = mysqli_query($con, $query);
								if(mysqli_num_rows($qryLista) > 0){
									$retorno['erro'] = 'Esse projeto possui um ou mais veículos vinculados!';
								}else{
									$query = "UPDATE projeto SET excluido = TRUE WHERE id =" . $id;
									mysqli_query($con, $query);
									if(!$con->error){
										$retorno['status'] = 1;
										gerarLog($con, $dados->usuario_sessao, "excluiu o projeto $id.");
									}
								}
							}
						}
					}
				}
			}
		}
	}
	echo json_encode($retorno);
}
