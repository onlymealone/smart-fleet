<?php
$camada = json_decode(file_get_contents('php://input'));
if(isset($camada)){
	$id = $camada->id;
	require_once('../conexao.php');
	$retorno = array();
	$retorno['status'] = 1;
	$query = "SELECT * FROM camada_projeto WHERE id_camada = " . $id;
	$qryLista = mysqli_query($con, $query);
	if(mysqli_num_rows($qryLista) > 0){
		$retorno['status'] = 0;
	}else{
		$query = "UPDATE camada SET excluido = TRUE WHERE id =" . $id;
		mysqli_query($con, $query);
		if($con->error){
			$retorno['status'] = 0;
		}else{
			gerarLog($con, $camada->usuario_sessao, "excluiu a camada $id.");
		}
	}
	echo json_encode($retorno);
}
