<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	$query = "DELETE FROM camada_projeto WHERE id_camada = $obj->id_camada AND id_projeto = $obj->id_projeto";
	mysqli_query($con, $query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $obj->usuario_sessao, "desvinculou a camada $obj->id_camada do projeto $obj->id_projeto.");
	}
	echo json_encode($retorno);
}
