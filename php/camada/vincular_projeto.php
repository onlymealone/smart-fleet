<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');
	$retorno['status'] = 1;
	$query = "INSERT INTO camada_projeto VALUES ($obj->id_camada, $obj->id_projeto)";
	mysqli_query($con, $query);
	if($con->error){
		$retorno['status'] = 0;
	}else{
		gerarLog($con, $obj->usuario_sessao, "vinculou a camada $obj->id_camada ao projeto $obj->id_projeto.");
	}

	echo json_encode($retorno);
}
