<?php
$obj = json_decode(file_get_contents('php://input'));
if(isset($obj)){
	require_once('../conexao.php');

	$query = "SELECT * FROM camada WHERE excluido = FALSE;";
	$qryLista = mysqli_query($con, $query);
	$retorno['status'] = 1;
	$retorno['camadas'] = array();
	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		$resultado['id_projeto'] = 0;
		$query2 = "SELECT * FROM camada_projeto WHERE id_camada = " . $resultado['id'] . " AND id_projeto = " . $obj->id_projeto;
		$qryLista2 = mysqli_query($con, $query2);
		if(mysqli_num_rows($qryLista2) > 0){
			$obj2 = mysqli_fetch_assoc($qryLista2);
			$resultado['id_projeto'] = $obj2['id_projeto'];
		}
		array_push($retorno['camadas'], $resultado);
	}
	echo json_encode($retorno);
}
