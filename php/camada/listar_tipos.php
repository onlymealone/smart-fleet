<?php

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	require_once('../conexao.php');

	$query = "SELECT * FROM tipo_camada;";
	$qryLista = mysqli_query($con, $query);
	$retorno = array();
	$retorno['status'] = 1;
	$retorno['tipo_camada'] = array();

	if(mysqli_num_rows($qryLista) == 0){
		$retorno['status'] = 0;
	}

	while($resultado = mysqli_fetch_assoc($qryLista)){
		array_push($retorno['tipo_camada'], $resultado);
	}
	echo json_encode($retorno);
}
