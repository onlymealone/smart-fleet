app.controller('OperadorVeiculoController', function($stateParams, $rootScope, $location, $scope, $state, Usuario, Veiculo, $modal){

    $scope.validarArray = function (obj) {
      if(angular.isArray(obj))
        return true;
      else
        return false;
    };

    $scope.abrirModal = function (vinculos) {
      $modal.open({
          templateUrl: 'pages/modal/padrao.html',
          controller: 'ModalController',
          resolve: {
              vinculos: function() {
                  return vinculos;
              }
          }
      });
    };

    $scope.vincular = function () {
      if($scope.oselecionado == ""){
        swal('Você deve selecionar um Operador', '', 'warning');
      }else{
        var obj = {operador: $scope.oselecionado, veiculo: $scope.vselecionado, usuario_sessao: $rootScope.global.usuario.id}
        Usuario.verificarVeiculo(obj)
         .success(function(data) {
           if(data.status){
             swal('Alterações salvas com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível salvar alterações!', "", "error");
           }
         }).error(function(erro) {
           swal('Erro ao criar vínculo!');
         });
       }
    };

    $scope.desvincular = function (id, tipo) {
      if(tipo == 'v'){
        Veiculo.desvincular({id: id})
         .success(function(data) {
           if(data.status){
             swal("Desvinculado com sucesso!", "", "success");
             $state.go($state.current, {}, {reload: true});
           }else{
            swal('Não foi possível desvincular operador!', "", "error");
           }
         }).error(function(erro) {
           swal('Erro ao desvincular operador!');
         });
      }else{
        swal({
              title: "Você tem certeza?",
              text: "Isso desvinculará TODOS os veículos relacionados à essa pessoa!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
              Usuario.desvincular({id: id})
               .success(function(data) {
                 if(data.status){
                   swal("Veículos desvinculados com sucesso!", "", "success");
                   $state.go($state.current, {}, {reload: true});
                 }else{
                   swal('Não foi possível desvincular veículos!');
                  }
                }).error(function(erro) {
                  swal('Erro ao desvincular veículos!');
                });
        });
      }
    };

    $scope.selecionar = function (obj, tipo) {
      if(tipo == 'o'){
          if(obj.id == $scope.oselecionado){
            $scope.oselecionado = "";
            $scope.vselecionado = [];
          }else{
            $scope.oselecionado = obj.id;
            if(obj.veiculos){
              $scope.vselecionado = obj.veiculos.slice(0);
            }else{
              $scope.vselecionado = [];
            }
            $scope.reordenarLista();
          }
      }else if(tipo == 'v' && $scope.oselecionado){
        var add = true;
        for(x in $scope.vselecionado){
          if(obj.id == $scope.vselecionado[x]){
            add = false;
            $scope.vselecionado.splice(x, 1);
          }
        }
        if(add){
          $scope.vselecionado.push(obj.id);
        }
      }
    };

    $scope.reordenarLista = function () {
      var novaLista = [];
      for(x in $scope.veiculos){
        var normal = true;
        for(y in $scope.vselecionado){
          if($scope.vselecionado[y] == $scope.veiculos[x].id){
            novaLista.unshift($scope.veiculos[x]);
            var normal = false;
          }
        }
        if(normal){
          novaLista.push($scope.veiculos[x]);
        }
      }
      $scope.veiculos = novaLista;
    };

    $scope.cancelar = function () {
      $scope.oselecionado = "";
      $scope.vselecionado = [];
    };

    $scope.verificarSelecionado = function (id, tipo) {
      var cor = '#00BCD4'
      if(tipo == 'o'){
        if(id == $scope.oselecionado){
          return cor;
        }
      }else if(tipo == 'v'){
        for(x in $scope.vselecionado){
          if(id == $scope.vselecionado[x]){
            return cor;
          }
        }
      }
      return 'white';
    };

    $scope.buscarOperadores = function () {
      Usuario.listarOperadores({projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.operadores = data.usuarios;
         }else{
          swal('Nenhum operador encontrado!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar operadores!');
       });
    };

    $scope.buscarVeiculos = function () {
      Veiculo.listarVeiculos({projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.veiculos = data.veiculos;
         }else{
          swal('Nenhum veículo encontrado!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar veículos!');
       });
    };

    (function initController() {
      $scope.oselecionado = "";
      $scope.vselecionado = [];
      $scope.buscarOperadores();
      $scope.buscarVeiculos();
    })();

});
