app.controller('CamadaController', function($stateParams, $rootScope, $location, $scope, $state, Camada){

    $scope.acao = function () {
      if($location.path() === '/camadas/nova'){
        $scope.adicionarCamada();
      }else{
        $scope.editarCamada();
      }
    };

    $scope.mostrarEsconderVinculados = function (mostrarVinculados) {
      if(!mostrarVinculados){
        $scope.camadas = $scope.bkp;
      }else{
        var novo = [];
        for(x in $scope.camadas){
          if($scope.camadas[x].id_projeto == $rootScope.global.projeto.id){
            novo.push($scope.camadas[x]);
          }
        }
        $scope.camadas = novo;
      }
    };

    $scope.adicionarCamada = function () {
      $scope.camada.id_tipo_camada = 0;
      if($scope.validarDados()){
        $scope.camada.usuario_sessao = $rootScope.global.usuario.id;
        Camada.cadastrar($scope.camada)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Camada cadastrada com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarCamadas');
                 }
               );
             }else{
               swal('Erro ao cadastrar camada!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar camada!');
         });
      }
    };

    $scope.editarCamada = function () {
      $scope.camada.id_tipo_camada = 0;
      if($scope.validarDados()){
        $scope.camada.usuario_sessao = $rootScope.global.usuario.id;
        Camada.editar($scope.camada)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Camada editada com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarCamadas');
                 }
               );
             }else{
               swal('Erro ao editar camada!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar camada!');
         });
      }
    };

    $scope.excluirCamada = function (id) {
      var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
      swal({
              title: "Você tem certeza?",
              text: "Essa camada será excluída PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                Camada.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Camada excluída com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir essa camada!", 'Essa camada está vinculada à um projeto!', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir camada!');
                 });
        });
    };

    $scope.vincularCamada = function (id_camada) {
      Camada.vincularProjeto({id_camada: id_camada, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Vinculada com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Erro ao vincular camada', '', 'error');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular camada!');
       });
    };

    $scope.desvincularCamada = function (id_camada) {
      Camada.desvincularProjeto({id_camada: id_camada, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Desvinculada com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível desvincular camada!', '', 'warning');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular camada!');
       });
    };

    $scope.listarCamadas = function () {
      Camada.listar({id_projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.camadas = data.camadas;
           $scope.bkp = data.camadas;
         }else{
          swal('Nenhuma camada encontrada!', '', 'warning');
         }
       }).error(function(erro) {
         swal('Erro ao buscar camadas!');
       });
    };

    $scope.buscarTiposCamadas = function () {
      Camada.listarTipos()
       .success(function(data) {
         if(data.status){
           $scope.tipo_camada = data.tipo_camada;
         }else{
           swal({
               title: "Nenhum tipo de camada cadastrado!", type:'error'
             },
             function(){
               $state.go('listarCamadas');
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar tipos de camada!');
       });
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.camada.id_tipo_camada)){
        idn = 'tipo_camada';
      }else if(angular.isUndefined($scope.camada.descricao) || $scope.camada.descricao == ""){
        idn = 'descricao';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'tipo_camada'){
          swal('O campo "Tipo" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    (function initController() {
      if($location.path() === '/camadas/listar'){
        $scope.listarCamadas();
      }else if($location.path() === '/camadas/nova'){
        $scope.buscarTiposCamadas();
        $scope.camada = {};
      }else if($location.path() === '/camadas/editar'){
        Camada.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.buscarTiposCamadas();
             $scope.camada = data.camada;
           }else{
             swal({
                 title: "Camada não encontrada!"
               },
               function(){
                 $state.go('listarCamadas');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar camada!"
             },
             function(){
               $state.go('listarCamadas');
             }
           );
         });
      }
    })();

});
