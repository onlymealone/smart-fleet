app.controller('ProjetoController', function($stateParams, $rootScope, $location, $scope, $state, Projeto){

    $scope.acao = function () {
      if($location.path() === '/projetos/novo'){
        $scope.adicionarProjeto();
      }else{
        $scope.editarProjeto();
      }
    };

    $scope.adicionarProjeto = function () {
      if($scope.validarDados()){
        $scope.projeto.id_pessoa = $rootScope.global.usuario.id;
        Projeto.cadastrar($scope.projeto)
         .success(function(response) {
           if(response != null) {
             if(response.status === 2){
               swal('Nome já cadastrado!', '', 'warning');
             }else{
               swal({
                   title: "Projeto cadastrado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarProjetos');
                 }
               );
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar projeto!');
         });
      }
    };

    $scope.editarProjeto = function () {
      if($scope.validarDados()){
        $scope.projeto.usuario_sessao = $rootScope.global.usuario.id;
        Projeto.editar($scope.projeto)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Projeto editado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarProjetos');
                 }
               );
             }else{
               swal("Nome já cadastrado!", '', 'warning');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar projeto!');
         });
      }
    };

    $scope.excluirProjeto = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Esse projeto será excluído PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Projeto.excluir(id)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Projeto excluído com sucesso!", type:"success"
                         },
                         function(){
                           if(!angular.isUndefined($rootScope.global.projeto)){
                             if(id.id == $rootScope.global.projeto.id){
                               $rootScope.global.projeto = null;
                               $state.go('home');
                             }else{
                               $state.go($state.current, {}, {reload: true});
                             }
                           }else{
                             $state.go($state.current, {}, {reload: true});
                           }
                         }
                       );
                     }else{
                       swal("Não foi possível excluir o projeto!", response.erro, 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir projeto!');
                 });
        });
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.projeto.nome) || $scope.projeto.nome == ""){
        campo = 'Nome';
        idn = 'nome';
      }else if(angular.isUndefined($scope.projeto.lote) || $scope.projeto.lote == ""){
        campo = 'Lote';
        idn = 'lote';
      }else if(angular.isUndefined($scope.projeto.extensao) || $scope.projeto.extensao == ""){
        campo = 'Extensão';
        idn = 'extensao';
      }else if(angular.isUndefined($scope.projeto.executor) || $scope.projeto.executor == ""){
        campo = 'Executor';
        idn = 'executor';
      }else if(angular.isUndefined($scope.projeto.prazo_contratual) || $scope.projeto.prazo_contratual == ""){
        campo = 'Prazo Contratual';
        idn = 'prazo_contratual';
      }else if(angular.isUndefined($scope.projeto.cidade) || $scope.projeto.cidade == ""){
        campo = 'Cidade';
        idn = 'cidade';
      }else if(angular.isUndefined($scope.projeto.Estado) || $scope.projeto.Estado == ""){
        campo = 'Estado';
        idn = 'Estado';
      }else{
        status = 1;
      }
      if(status == 0){
        document.getElementById(idn).focus();
        return false;
      }else{
        return true;
      }
    };

    $scope.listarProjetos = function () {
      if($rootScope.global.usuario.id_tipo_pessoa == 2){
        Projeto.listar()
         .success(function(data) {
           if(data.status){
             $scope.projetos = data.projetos;
           }else{
            swal('Nenhum projeto encontrado!', '', 'warning');
           }
         }).error(function(erro) {
           swal('Erro ao buscar projetos!');
         });
      }else{
        Projeto.listarPorUsuario($rootScope.global.usuario.id)
         .success(function(data) {
           if(data.status){
             $scope.projetos = data.projetos;
           }else{
            swal('Nenhum projeto encontrado!', '', 'warning');
           }
         }).error(function(erro) {
           swal('Erro ao buscar projetos!');
         });
       }
    };

    $scope.buscarProximoIdProjeto = function () {
      Projeto.buscarProximoId()
       .success(function(data) {
         if(data.status){
           $scope.projeto.id = data.id;
         }else{
          swal('Não foi possível recuperar id do projeto!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar id do projeto!');
       });
    }

    $scope.selecionar = function (projeto) {
      $rootScope.global.projeto = projeto;
      $state.go('home');
    };

    (function initController() {
      if($location.path() === '/projetos/listar'){
        $scope.listarProjetos();
      }else if($location.path() === '/projetos/novo'){
        $scope.projeto = {};
        $scope.buscarProximoIdProjeto();
      }else if($location.path() === '/projetos/editar'){
        $scope.editar = 1;
        Projeto.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.projeto = data.projeto;
           }else{
             swal({
                 title: "Projeto não encontrado!"
               },
               function(){
                 $state.go('listarProjetos');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar projeto!"
             },
             function(){
               $state.go('listarProjetos');
             }
           );
         });
      }
    })();

});
