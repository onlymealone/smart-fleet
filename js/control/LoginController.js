app.controller('LoginController', function($timeout, $rootScope, $location, $scope, $cookies, $state, AuthService, Usuario){
    (function initController() {
      $scope.usuario = {};
      if($location.path() === '/login'){
        $scope.form = 'login';
      }else{
        $scope.form = 'eas';
      }
    })();

    $scope.recuperarSenha = function (email) {
      if(!angular.isUndefined(email) && email != ''){
        var obj = {email: email, baseUrl: $rootScope.baseUrl};
        $scope.spinner = true;
        Usuario.recuperarSenha(obj)
         .success(function(response) {
         $scope.spinner = false;
           if(response != null) {
             if(response.status){
               swal({
                   title: '', text: "Um e-mail foi enviado para você com as instruções para redefinir sua senha!", type:"success"
                 },
                 function(){
                   $state.go('login');
                 }
               );
             }else{
               swal("E-mail não encontrado!", '', 'warning');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
         $scope.spinner = false;
           swal('Erro ao alterar a senha!');
         });
      }else{
        $('#email').focus();
      }
    };

    $scope.fazerLogin = function () {
      if(angular.isUndefined($scope.usuario.login)){
        swal("Preencha o campo 'Login'");
        $('#usuario').focus();
      }else if(angular.isUndefined($scope.usuario.senha)){
        swal("Preencha o campo 'Senha'");
        $('#senha').focus();
      }else{
        AuthService.Login($scope.usuario, function(response) {
            if(response != null) {
                if(!response.status){
                  swal("Login ou senha incorretos!");
                }else{
                  AuthService.AtualizarCredenciais(response.usuario);
                  $state.go('home');
                }
            } else {
            	swal("ERRO");
            }
        });
      }
    };

});
