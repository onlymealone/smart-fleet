app.controller('MaterialController', function($stateParams, $rootScope, $location, $scope, $state, Material){

    $scope.acao = function () {
      if($location.path() === '/materiais/novo'){
        $scope.adicionarMaterial();
      }else{
        $scope.editarMaterial();
      }
    };

    $scope.mostrarEsconderVinculados = function (mostrarVinculados) {
      if(!mostrarVinculados){
        $scope.materiais = $scope.bkp;
      }else{
        var novo = [];
        for(x in $scope.materiais){
          if($scope.materiais[x].id_projeto == $rootScope.global.projeto.id){
            novo.push($scope.materiais[x]);
          }
        }
        $scope.materiais = novo;
      }
    };

    $scope.adicionarMaterial = function () {
      if($scope.validarDados()){
        $scope.material.usuario_sessao = $rootScope.global.usuario.id;
        Material.cadastrar($scope.material)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Material cadastrado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarMateriais');
                 }
               );
             }else{
               swal('Erro ao cadastrar material!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar material!');
         });
      }
    };

    $scope.editarMaterial = function () {
      if($scope.validarDados()){
        $scope.material.usuario_sessao = $rootScope.global.usuario.id;
        Material.editar($scope.material)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Material editado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarMateriais');
                 }
               );
             }else{
               swal('Erro ao editar material!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar material!');
         });
      }
    };

    $scope.excluirMaterial = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Esse material será excluído PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Material.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Material excluído com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir esse material!", 'Esse material está vinculado à um projeto!', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir material!');
                 });
        });
    };

    $scope.vincularMaterial = function (id_material) {
      Material.vincularProjeto({id_material: id_material, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Vinculado com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Erro ao vincular material', '', 'error');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular material!');
       });
    };

    $scope.desvincularMaterial = function (id_material) {
      Material.desvincularProjeto({id_material: id_material, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Desvinculado com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível desvincular material!', '', 'warning');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular material!');
       });
    };

    $scope.listarMateriais = function () {
      Material.listar({id_projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.materiais = data.materiais;
           $scope.bkp = data.materiais;
         }else{
          swal('Nenhum material encontrada!', '', 'warning');
         }
       }).error(function(erro) {
         swal('Erro ao buscar materiais!');
       });
    };

    $scope.buscarUnidades = function () {
      Material.listarUnidades()
       .success(function(data) {
         if(data.status){
           $scope.unidades = data.unidades;
         }else{
           swal({
               title: "Nenhuma unidade cadastrada!", type:'error'
             },
             function(){
               $state.go('listarMateriais');
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar unidades!');
       });
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.material.id_unidade)){
        idn = 'unidade';
      }else if(angular.isUndefined($scope.material.descricao) || $scope.material.descricao == ""){
        idn = 'descricao';
      }else if(angular.isUndefined($scope.material.densidade) || $scope.material.densidade == ""){
        idn = 'densidade';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'unidade'){
          swal('O campo "Unidade" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    (function initController() {
      if($location.path() === '/materiais/listar'){
        $scope.listarMateriais();
      }else if($location.path() === '/materiais/novo'){
        $scope.buscarUnidades();
        $scope.material = {};
      }else if($location.path() === '/materiais/editar'){
        Material.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.buscarUnidades();
             $scope.material = data.material;
             $scope.material.complemento_manual = $scope.material.insercao_manual == 1 ? true : false;
             $scope.material.densidade = Number($scope.material.densidade);
           }else{
             swal({
                 title: "Material não encontrado!"
               },
               function(){
                 $state.go('listarMateriais');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar material!"
             },
             function(){
               $state.go('listarMateriais');
             }
           );
         });
      }
    })();

});
