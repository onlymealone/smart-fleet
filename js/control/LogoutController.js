app.controller('LogoutController', function($state, AuthService){
    (function initController() {
      AuthService.LimparCredenciais();
      $state.go('login');
    })();
});
