app.controller("HomeController", function ($rootScope, $scope, $http, $state, Projeto) {

			(function initController() {
				if($rootScope.global.usuario.id_tipo_pessoa == 55){
					if($rootScope.global.usuario.id_projeto == 0){
						swal({
									title: "ERRO",
									text: "Você não está associado à nenhum projeto e não tem acesso ao aplicativo!",
									type: "error",
									confirmButtonColor: "#DD6B55",
									confirmButtonText: "Confirmar",
									closeOnConfirm: false });
						$state.go('logout');
					}
					Projeto.buscar($rootScope.global.usuario.id_projeto)
	         .success(function(response) {
	           if(response != null) {
	             if(response.status){
								 $rootScope.global.projeto = response.projeto;
	             }else{
							 	swal("ERRO");
	             }
	           } else {
	             swal("ERRO");
	           }
	         }).error(function(erro) {
	           swal('Erro ao buscar projeto!');
	         });
				}
			})();

		})
