app.controller('LogController', function($scope, Log){

    $scope.buscarLogs = function () {
      Log.listar().success(function(data) {
        if(data.status){
          $scope.logs = data.logs;
        }else{
         swal('Nenhum log encontrado!', '', 'warning');
        }
      }).error(function(erro) {
        swal('Erro ao buscar logs!');
      });
    };

    (function initController() {
      $scope.buscarLogs();
    })();

});
