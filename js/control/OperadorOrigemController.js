app.controller('OperadorOrigemController', function($stateParams, $rootScope, $location, $scope, $state, Usuario, Origem, $modal){

    $scope.validarArray = function (obj) {
      if(angular.isArray(obj))
        return true;
      else
        return false;
    };

    $scope.abrirModal = function (vinculos) {
      $modal.open({
          templateUrl: 'pages/modal/padrao.html',
          controller: 'ModalController',
          resolve: {
              vinculos: function() {
                  return vinculos;
              }
          }
      });
    };

    $scope.vincular = function () {
      if($scope.oselecionado == ""){
        swal('Você deve selecionar um Operador', '', 'warning');
      }else{
        var obj = {operador: $scope.oselecionado, origem: $scope.ocselecionado, usuario_sessao: $rootScope.global.usuario.id}
        Usuario.verificarOrigem(obj)
         .success(function(data) {
           if(data.status){
             swal('Alterações salvas com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível salvar alterações!', "", "error");
           }
         }).error(function(erro) {
           swal('Erro ao criar vínculo!');
         });
       }
    };

    $scope.desvincular = function (id, tipo) {
        swal({
              title: "Você tem certeza?",
              text: "Isso desvinculará TODAS as origens relacionadas à essa pessoa!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
              Usuario.desvincularOrigem({id: id})
               .success(function(data) {
                 if(data.status){
                   swal("Origens desvinculadas com sucesso!", "", "success");
                   $state.go($state.current, {}, {reload: true});
                 }else{
                   swal('Não foi possível desvincular origens!');
                  }
                }).error(function(erro) {
                  swal('Erro ao desvincular origens!');
                });
        });
    };

    $scope.selecionar = function (obj, tipo) {
      if(tipo == 'o'){
          if(obj.id == $scope.oselecionado){
            $scope.oselecionado = "";
            $scope.ocselecionado = [];
          }else{
            $scope.oselecionado = obj.id;
            if(obj.origens){
              $scope.ocselecionado = obj.origens.slice(0);
            }else{
              $scope.ocselecionado = [];
            }
            $scope.reordenarLista();
          }
      }else if(tipo == 'oc' && $scope.oselecionado){
        var add = true;
        for(x in $scope.ocselecionado){
          if(obj.id == $scope.ocselecionado[x]){
            add = false;
            $scope.ocselecionado.splice(x, 1);
          }
        }
        if(add){
          $scope.ocselecionado.push(obj.id);
        }
      }
    };

    $scope.reordenarLista = function () {
      var novaLista = [];
      for(x in $scope.origens){
        var normal = true;
        for(y in $scope.ocselecionado){
          if($scope.ocselecionado[y] == $scope.origens[x].id){
            novaLista.unshift($scope.origens[x]);
            var normal = false;
          }
        }
        if(normal){
          novaLista.push($scope.origens[x]);
        }
      }
      $scope.origens = novaLista;
    };

    $scope.cancelar = function () {
      $scope.oselecionado = "";
      $scope.ocselecionado = [];
    };

    $scope.verificarSelecionado = function (id, tipo) {
      var cor = '#00BCD4'
      if(tipo == 'o'){
        if(id == $scope.oselecionado){
          return cor;
        }
      }else if(tipo == 'oc'){
        for(x in $scope.ocselecionado){
          if(id == $scope.ocselecionado[x]){
            return cor;
          }
        }
      }
      return 'white';
    };

    $scope.buscarOperadores = function () {
      Usuario.listarOperadoresOrigens({projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.operadores = data.usuarios;
         }else{
          swal('Nenhum operador encontrado!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar operadores!');
       });
    };

    $scope.buscarOrigens = function () {
      Origem.listarOrigens({projeto: $rootScope.global.projeto.id})
       .success(function(data) {
         if(data.status){
           $scope.origens = data.origens;
         }else{
          swal('Nenhuma origem encontrada!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar origem!');
       });
    };

    (function initController() {
      $scope.oselecionado = "";
      $scope.ocselecionado = [];
      $scope.buscarOperadores();
      $scope.buscarOrigens();
    })();

});
