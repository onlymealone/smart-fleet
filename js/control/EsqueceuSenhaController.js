app.controller('EsqueceuSenhaController', function($stateParams, $rootScope, $location, $scope, $state, Usuario){

    $scope.alterarSenhaES = function () {
      if($scope.validarDados()){
        var obj = {hash: $stateParams.hash, senha: $scope.senha};
        Usuario.alterarSenha(obj)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Senha alterada com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('login');
                 }
               );
             }else{
               swal("Não foi possível alterar a senha! Tente novamente mais tarde.", '', 'warning');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao alterar a senha!');
         });
      }
    };

    $scope.validarDados = function () {
      var status = 0;
      if(angular.isUndefined($scope.senha) || $scope.senha == ""){
        idn = 'senha';
      }else if(angular.isUndefined($scope.csenha) || $scope.csenha == ""){
        idn = 'csenha';
      }else if($scope.senha != $scope.csenha){
        swal('O campo "Confirmar nova senha" deve ser igual ao campo "Nova Senha"', '', 'warning');
        idn = 'csenha';
      }else{
        status = 1;
      }
      if(status == 0){
        document.getElementById(idn).focus();
        return false;
      }else{
        return true;
      }
    };

    $scope.buscarUsuario = function () {
      Usuario.buscarHash({hash: $stateParams.hash})
       .success(function(data) {
         if(!data.status){
           swal({
               title: "Esse link expirou ou não existe!"
             },
             function(){
               $state.go('login');
             }
           );
         }
       }).error(function(erro) {
         swal({
             title: "Erro!"
           },
           function(){
             $state.go('login');
           }
         );
       });
    };

    (function initController() {
        $scope.buscarUsuario();
    })();

});
