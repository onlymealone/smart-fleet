app.controller('OrigemController', function($stateParams, $rootScope, $location, $scope, $state, Origem){

    $scope.acao = function () {
      if($location.path() === '/origens/nova'){
        $scope.adicionarOrigem();
      }else{
        $scope.editarOrigem();
      }
    };

    $scope.mostrarEsconderVinculados = function (mostrarVinculados) {
      if(!mostrarVinculados){
        $scope.origens = $scope.bkp;
      }else{
        var novo = [];
        for(x in $scope.origens){
          if($scope.origens[x].id_projeto == $rootScope.global.projeto.id){
            novo.push($scope.origens[x]);
          }
        }
        $scope.origens = novo;
      }
    };

    $scope.adicionarOrigem = function () {
      if($scope.validarDados()){
        if(!angular.isUndefined($rootScope.global.projeto)){
          $scope.origem.id_projeto = $rootScope.global.projeto.id;
        }else{
          $scope.origem.id_projeto = "";
        }
        $scope.origem.usuario_sessao = $rootScope.global.usuario.id;
        Origem.cadastrar($scope.origem)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Origem cadastrada com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarOrigens');
                 }
               );
             }else{
               swal('Erro ao cadastrar origem!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar origem!');
         });
      }
    };

    $scope.editarOrigem = function () {
      if($scope.validarDados()){
        $scope.origem.usuario_sessao = $rootScope.global.usuario.id;
        Origem.editar($scope.origem)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Origem editada com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarOrigens');
                 }
               );
             }else{
               swal('Erro ao editar origem!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar origem!');
         });
      }
    };

    $scope.excluirOrigem = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Essa origem será excluída PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Origem.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Origem excluída com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir essa origem!", '', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir origem!');
                 });
        });
    };

    $scope.vincularOrigem = function (origem) {
      if(origem.id_projeto == "" || origem.id_projeto == null || angular.isUndefined(origem.id_projeto) || origem.id_projeto == 0){
        Origem.vincularProjeto({id_origem: origem.id, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal('Vinculada com sucesso!', '', 'success');
               $state.go($state.current, {}, {reload: true});
             }else{
               swal('Erro ao vincular origem', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao vincular origem!');
         });
       }else{
         swal('Origem já vinculada!', 'Essa origem já está vinculada à outro projeto! Favor, desvincular antes de tentar vincular à esse projeto.', 'warning');
       }
    };

    $scope.desvincularOrigem = function (id_origem) {
      Origem.desvincularProjeto({id_origem: id_origem, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Desvinculada com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Erro ao desvincular origem!', '', 'error');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular origem!');
       });
    };

    $scope.gerarQRCodeOrigem = function (origem) {
      var obj = {qrcode: '{"chave": "' + origem.id + origem.descricao + '"}'};
      Origem.baixarQRCode(obj)
       .success(function(data) {
          document.getElementById('imprimir').innerHTML = '<img width="100%" src="data:image/jpeg;base64, ' + data.imagem + '">';
          setTimeout(function () {
            window.print();
          }, 100);
          // var blob = $scope.b64toBlob(data.imagem);
          // var blobUrl = URL.createObjectURL(blob);
          // var anchor = document.createElement('a');
          // anchor.href = blobUrl;
          // anchor.target = '_blank';
          // anchor.download = 'QRCode origem ID ' + origem.id;
          // anchor.click();
       }).error(function(erro) {
         swal('Erro ao gerar QRCode!');
       });
    };

    $scope.listarOrigens = function () {
      Origem.listar()
       .success(function(data) {
         if(data.status){
           $scope.origens = data.origens;
           $scope.bkp = data.origens;
         }else{
          swal('Nenhuma origem encontrada!', '', 'warning');
         }
       }).error(function(erro) {
         swal('Erro ao buscar origens!');
       });
    };

    $scope.buscarTiposOrigens = function () {
      Origem.listarTipos()
       .success(function(data) {
         if(data.status){
           $scope.tipo_origem = data.tipo_origem;
         }else{
           swal({
               title: "Nenhum tipo de origem cadastrado!", type:'error'
             },
             function(){
               $state.go('listarOrigens');
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar tipos de origem!');
       });
    };

    $scope.clonarOrigem = function (origem) {
      origem.id = "";
      $scope.origem = origem;
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.origem.id_tipo_origem)){
        idn = 'tipo_origem';
      }else if(angular.isUndefined($scope.origem.descricao) || $scope.origem.descricao == "" || $scope.origem.descricao == null){
        idn = 'descricao';
      }else if(angular.isUndefined($scope.origem.modelo) || $scope.origem.modelo == "" || $scope.origem.modelo == null){
        idn = 'modelo';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'tipo_origem'){
          swal('O campo "Tipo" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    $scope.b64toBlob = function (b64Data, sliceSize) {
      contentType = 'image/png';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };

    (function initController() {
      if($location.path() === '/origens/listar'){
        $scope.listarOrigens();
      }else if($location.path() === '/origens/nova'){
        $scope.buscarTiposOrigens();
        if($stateParams.origem){
          $scope.clonarOrigem($stateParams.origem);
        }else{
          $scope.origem = {};
        }
      }else if($location.path() === '/origens/editar'){
        Origem.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.buscarTiposOrigens();
             $scope.origem = data.origem;
           }else{
             swal({
                 title: "Origem não encontrada!"
               },
               function(){
                 $state.go('listarOrigens');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar origem!"
             },
             function(){
               $state.go('listarOrigens');
             }
           );
         });
      }
    })();

});
