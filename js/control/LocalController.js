app.controller('LocalController', function($stateParams, $rootScope, $location, $scope, $state, Local, NgMap){
    var vm = this;
    var marker;
    $scope.local = {};
    $scope.lat = -14.235004;
    $scope.lng = -51.92528;
    $scope.zoom = 4;
    NgMap.getMap().then(function(map) {
      vm.map = map;
    });
    vm.placeMarker = function(e) {
      if ( marker ) {
        marker.setPosition(e.latLng);
      } else {
        marker = new google.maps.Marker({
          position: e.latLng,
          map: vm.map
        });
      }
      $scope.local.latitude = e.latLng.lat();
      $scope.local.longitude = e.latLng.lng();
    }

    $scope.acao = function () {
      if($location.path() === '/locais/novo'){
        $scope.adicionarLocal();
      }else{
        $scope.editarLocal();
      }
    };

    $scope.importarArquivoEstacas = function () {
      if(angular.isUndefined($scope.id_local_carga)){
        swal('O campo "Local" deve ser preenchido', '', 'warning');
      }else if(angular.isUndefined($scope.arquivo)){
        swal('Você deve selecionar um arquivo!', '', 'warning');
      }else{
        if($scope.arquivo.type == 'text/plain'){
          var txt = $scope.arquivo.data.split(',');
          var registros = atob(txt[1]);
          registros = registros.split('\n');
          var cbc = registros[0].split(';');
          if(cbc[0].trim() == "Descricao" && cbc[1].trim() == "Lat" && cbc[2].trim() == "Lon"){
            registros.shift();
            var obj = {id_local_carga: $scope.id_local_carga, registros: registros, usuario_sessao: $rootScope.global.usuario.id}
            Local.cadastrarEstacas(obj)
             .success(function(response) {
               if(response != null) {
                 if(response.status){
                   swal({
                       title: "Arquivo importado com sucesso!", type:"success"
                     },
                     function(){
                       $state.go('listarLocais');
                     }
                   );
                 }else{
                   swal('Erro ao importar arquivo!', '', 'error');
                 }
               } else {
                 swal("ERRO");
               }
             }).error(function(erro) {
               swal('Erro ao importar arquivo!');
             });
          }else{
            swal('Cabeçalho do arquivo inválido! O cabeçalho deve seguir o padrão (Descricao;Lat;Lon;)', '', 'error');
          }
        }else{
          swal('São aceitos APENAS arquivos de texto (.txt)', '', 'warning');
        }
      }
    };

    $scope.adicionarLocal = function () {
      if($scope.validarDados()){
        $scope.local.id_projeto = $rootScope.global.projeto.id;
        $scope.local.usuario_sessao = $rootScope.global.usuario.id;
        Local.cadastrar($scope.local)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Local cadastrado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarLocais');
                 }
               );
             }else{
               swal('Erro ao cadastrar local!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar local!');
         });
      }
    };

    $scope.editarLocal = function () {
      if($scope.validarDados()){
        $scope.local.usuario_sessao = $rootScope.global.usuario.id;
        Local.editar($scope.local)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal({
                   title: "Local editado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarLocais');
                 }
               );
             }else{
               swal('Erro ao editar local!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar local!');
         });
      }
    };

    $scope.excluirLocal = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Esse local será excluído PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Local.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Local excluído com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir esse local!", '', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir local!');
                 });
        });
    };

    $scope.listarLocais = function () {
      Local.listar($rootScope.global.projeto.id)
       .success(function(data) {
         if(data.status){
           $scope.locais = data.locais;
         }else{
          swal('Nenhum local encontrado!', '', 'warning');
         }
       }).error(function(erro) {
         swal('Erro ao buscar locais!');
       });
    };

    $scope.buscarTiposLocais = function () {
      Local.listarTipos()
       .success(function(data) {
         if(data.status){
           $scope.tipo_local = data.tipo_local;
         }else{
           swal({
               title: "Nenhum tipo de local cadastrado!", type:'error'
             },
             function(){
               $state.go('listarLocais');
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar tipos de local!');
       });
    };

    $scope.clonarLocal = function (local) {
      local.id = "";
      local.complemento_manual = local.complemento_manual == 1 ? true : false;
      $scope.local = local;
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.local.id_tipo_local)){
        idn = 'tipo_local';
      }else if(angular.isUndefined($scope.local.descricao) || $scope.local.descricao == ""){
        idn = 'descricao';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'tipo_local'){
          swal('O campo "Tipo" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    (function initController() {
      if($location.path() === '/locais/listar' || $location.path() === '/locais/estacas/importar'){
        if(angular.isUndefined($rootScope.global.projeto)){
          swal({
              title: "Nenhum projeto foi selecionado!"
            },
            function(){
              $state.go('home');
            }
          );
        }else{
          $scope.listarLocais();
        }
      }else if($location.path() === '/locais/novo'){
        $scope.buscarTiposLocais();
        if($stateParams.local){
          $scope.clonarLocal($stateParams.local);
          $scope.zoom = (!$scope.local.latitude || $scope.local.latitude == 0 || $scope.local.latitude == null || $scope.local.latitude == "" ? 4 : 17);
          $scope.local.latitude = (!$scope.local.latitude || $scope.local.latitude == 0 || $scope.local.latitude == null || $scope.local.latitude == "" ? -14.235004 : $scope.local.latitude);
          $scope.local.longitude = (!$scope.local.longitude || $scope.local.longitude == 0 || $scope.local.longitude == null || $scope.local.longitude == "" ? -51.92528 : $scope.local.longitude);
          $scope.lat = $scope.local.latitude;
          $scope.lng = $scope.local.longitude;
        }else{
          $scope.local = {};
          $scope.lat, $scope.local.latitude = -14.235004;
          $scope.lng, $scope.local.longitude = -51.92528;
          $scope.zoom = 4;
        }
      }else if($location.path() === '/locais/editar'){
        Local.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.buscarTiposLocais();
             $scope.local = data.local;
             $scope.local.complemento_manual = $scope.local.complemento_manual == 1 ? true : false;
             $scope.zoom = (!$scope.local.latitude || $scope.local.latitude == 0 || $scope.local.latitude == null || $scope.local.latitude == "" ? 4 : 17);
             $scope.local.latitude = (!$scope.local.latitude || $scope.local.latitude == 0 || $scope.local.latitude == null || $scope.local.latitude == "" ? -14.235004 : $scope.local.latitude);
             $scope.local.longitude = (!$scope.local.longitude || $scope.local.longitude == 0 || $scope.local.longitude == null || $scope.local.longitude == "" ? -51.92528 : $scope.local.longitude);
             $scope.lat = $scope.local.latitude;
             $scope.lng = $scope.local.longitude;
           }else{
             swal({
                 title: "Local não encontrado!"
               },
               function(){
                 $state.go('listarLocais');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar local!"
             },
             function(){
               $state.go('listarLocais');
             }
           );
         });
      }
    })();

});
