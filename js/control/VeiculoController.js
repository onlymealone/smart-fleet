app.controller('VeiculoController', function($stateParams, $rootScope, $location, $scope, $state, Veiculo, Terceiro, $modal){

    $scope.acao = function () {
      if($location.path() === '/veiculos/novo'){
        $scope.adicionarVeiculo();
      }else{
        $scope.editarVeiculo();
      }
    };

    $scope.mostrarEsconderVinculados = function (mostrarVinculados) {
      if(!mostrarVinculados){
        $scope.veiculos = $scope.bkp;
      }else{
        var novo = [];
        for(x in $scope.veiculos){
          if($scope.veiculos[x].id_projeto == $rootScope.global.projeto.id){
            novo.push($scope.veiculos[x]);
          }
        }
        $scope.veiculos = novo;
      }
    };

    $scope.adicionarVeiculo = function () {
      if($scope.validarDados()){
        if(!angular.isUndefined($rootScope.global.projeto)){
          $scope.veiculo.id_projeto = $rootScope.global.projeto.id;
        }else{
          $scope.veiculo.id_projeto = "";
        }
        if(!$scope.mterceiro){
          $scope.veiculo.id_terceiro = 0;
        }
        $scope.veiculo.usuario_sessao = $rootScope.global.usuario.id;
        Veiculo.cadastrar($scope.veiculo)
         .success(function(response) {
           if(response != null) {
             if(response.status === 2){
               swal('Código já cadastrado!', '', 'warning');
             }else if(response.status === 3){
               swal('Placa já cadastrada!', '', 'warning');
             }else if(response.status === 0){
               swal('Erro ao cadastrar veículo!', '', 'error');
             }else{
               if(!angular.isUndefined($rootScope.global.projeto)){
                 swal({
                         title: "Veículo cadastrado com sucesso!",
                         text: "Você deseja ir para a tela de vínculo de Operador x Veículo?",
                         type: "success",
                         showCancelButton: true,
                         cancelButtonText: "NÃO",
                         confirmButtonColor: "green",
                         confirmButtonText: "SIM",
                         closeOnConfirm: true },
                         function(isConfirm){
                           if(isConfirm){
                             $state.go('listarOpV');
                          }else{
                            $state.go('listarVeiculos');
                          }
                   });
                }else{
                  swal({
                      title: "Veículo cadastrado com sucesso!", type:"success"
                    },
                    function(){
                      $state.go('listarVeiculos');
                    }
                  );
                }
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar veículo!');
         });
      }
    };

    $scope.editarVeiculo = function () {
      if($scope.validarDados()){
        if(!$scope.mterceiro){
          $scope.veiculo.id_terceiro = 0;
        }
        $scope.veiculo.usuario_sessao = $rootScope.global.usuario.id;
        Veiculo.editar($scope.veiculo)
         .success(function(response) {
           if(response != null) {
             if(response.status === 2){
               swal('Código já cadastrado!', '', 'warning');
             }else if(response.status === 3){
               swal('Placa já cadastrada!', '', 'warning');
             }else if(response.status === 0){
               swal('Ocorreu um erro ao editar o veículo!', '', 'error');
             }else{
               swal({
                   title: "Veículo editado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarVeiculos');
                 }
               );
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar veículo!');
         });
      }
    };

    $scope.excluirVeiculo = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Esse veículo será excluído PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Veiculo.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Veículo excluído com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir esse veículo!", 'Esse veículo está vinculado a um operador. Desvincule antes de excluir', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir veículo!');
                 });
        });
    };

    $scope.vincularVeiculo = function (veiculo) {
      if(veiculo.id_projeto == "" || veiculo.id_projeto == null || angular.isUndefined(veiculo.id_projeto) || veiculo.id_projeto == 0){
        Veiculo.vincularProjeto({id_veiculo: veiculo.id, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal('Vinculado com sucesso!', '', 'success');
               $state.go($state.current, {}, {reload: true});
             }else{
               swal('Erro ao vincular veículo', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao vincular veículo!');
         });
       }else{
         swal('Veículo já vinculado!', 'Esse veículo já está vinculado à outro projeto! Favor, desvincular antes de tentar vincular à esse projeto.', 'warning');
       }
    };

    $scope.desvincularVeiculo = function (id_veiculo) {
      Veiculo.desvincularProjeto({id_veiculo: id_veiculo, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Desvinculado com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível desvincular veículo!', 'Esse veículo está vinculado à um usuário', 'warning');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular veículo!');
       });
    };

    $scope.gerarQRCodeVeiculo = function (veiculo) {
      var obj = {qrcode: '{"id": ' + veiculo.id + ', "codigo": "' + veiculo.codigo + '", "placa": "' + veiculo.placa + '"}'};
      Veiculo.baixarQRCode(obj)
       .success(function(data) {
          document.getElementById('imprimir').innerHTML = '<img width="100%" src="data:image/jpeg;base64, ' + data.imagem + '">';
          setTimeout(function () {
              window.print();
          }, 100);
          // var blob = $scope.b64toBlob(data.imagem);
          // var blobUrl = URL.createObjectURL(blob);
          // var anchor = document.createElement('a');
          // anchor.href = blobUrl;
          // anchor.target = '_blank';
          // anchor.download = 'QRCode veículo ID ' + veiculo.id;
          // anchor.click();
       }).error(function(erro) {
         swal('Erro ao gerar QRCode!');
       });
    };

    $scope.listarVeiculos = function () {
      // Veiculo.listar($rootScope.global.projeto.id)
      Veiculo.listar()
       .success(function(data) {
         if(data.status){
           $scope.veiculos = data.veiculos;
           $scope.bkp = data.veiculos;
         }else{
          swal('Nenhum veículo encontrado!', '', 'warning');
         }
       }).error(function(erro) {
         swal('Erro ao buscar veículos!');
       });
    };

    $scope.buscarTiposVeiculo = function () {
      Veiculo.listarTipos()
       .success(function(data) {
         if(data.status){
           $scope.tipo_veiculo = data.tipo_veiculo;
         }else{
           swal({
               title: "Nenhum tipo de veículo cadastrado!", type:'error'
             },
             function(){
               $state.go('listarVeiculos');
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar tipos de veículos!');
       });
    };

    $scope.buscarTerceiros = function () {
      Terceiro.listar()
       .success(function(data) {
         if(data.status){
           $scope.terceiros = data.terceiros;
         }else{
           swal({
               title: "Nenhum terceiro cadastrado!", type:'warning'
             },
             function(){
               $scope.mterceiro = false;
             }
           );
         }
       }).error(function(erro) {
         swal('Erro ao buscar terceiros!');
       });
    };

    $scope.clonarVeiculo = function (veiculo) {
      veiculo.id = "";
      veiculo.codigo = "";
      veiculo.placa = "";
      $scope.veiculo = veiculo;
      $scope.veiculo.capacidade = Number(veiculo.capacidade);
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.veiculo.id_tipo_veiculo)){
        idn = 'tipo_veiculo';
      }else if((angular.isUndefined($scope.veiculo.id_terceiro) || $scope.veiculo.id_terceiro == "" || $scope.veiculo.id_terceiro == null) && $scope.mterceiro){
        idn = 'id_terceiro';
      }else if(angular.isUndefined($scope.veiculo.codigo) || $scope.veiculo.codigo == ""){
        idn = 'codigo';
      }else if(angular.isUndefined($scope.veiculo.placa) || $scope.veiculo.placa == ""){
        idn = 'placa';
      }else if(angular.isUndefined($scope.veiculo.marca) || $scope.veiculo.marca == ""){
        idn = 'marca';
      }else if(angular.isUndefined($scope.veiculo.modelo) || $scope.veiculo.modelo == ""){
        idn = 'modelo';
      }else if(angular.isUndefined($scope.veiculo.capacidade) || $scope.veiculo.capacidade == "" || $scope.veiculo.capacidade == 0){
        idn = 'capacidade';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'tipo_veiculo'){
          swal('O campo "Tipo Veículo" deve ser preenchido', '', 'warning');
        }else if(idn == 'id_terceiro'){
          swal('O campo "Terceiro" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    $scope.b64toBlob = function (b64Data, sliceSize) {
      contentType = 'image/png';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };

    (function initController() {
      if($location.path() === '/veiculos/listar'){
        // if(angular.isUndefined($rootScope.global.projeto)){
        //   swal({
        //       title: "Nenhum projeto foi selecionado!"
        //     },
        //     function(){
        //       $state.go('home');
        //     }
        //   );
        // }else{
          $scope.listarVeiculos();
        // }
      }else if($location.path() === '/veiculos/novo'){
        $scope.buscarTiposVeiculo();
        $scope.buscarTerceiros();
        if($stateParams.veiculo){
          $scope.clonarVeiculo($stateParams.veiculo);
        }else{
          $scope.veiculo = {};
        }
      }else if($location.path() === '/veiculos/editar'){
        Veiculo.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.buscarTiposVeiculo();
             $scope.buscarTerceiros();
             $scope.veiculo = data.veiculo;
             $scope.veiculo.capacidade = Number(data.veiculo.capacidade);
             if($scope.veiculo.id_terceiro != 0){
               $scope.mterceiro = true;
             }else{
               $scope.veiculo.id_terceiro = null;
             }
           }else{
             swal({
                 title: "Veículo não encontrado!"
               },
               function(){
                 $state.go('listarVeiculos');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar veículo!"
             },
             function(){
               $state.go('listarVeiculos');
             }
           );
         });
      }
    })();

});
