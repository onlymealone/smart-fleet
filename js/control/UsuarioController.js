app.controller('UsuarioController', function($stateParams, $timeout, $rootScope, $location, $scope, $cookies, $state, Usuario){
    $scope.usuario;
    $scope.phoneMask = "(99) 9999-9999";
    $scope.listaCracha = [];
    $scope.checkboxes = [];

    $scope.acao = function () {
      if($location.path() === '/usuario/adicionar'){
        $scope.adicionarUsuario();
      }else{
        $scope.editarUsuario();
      }
    };

    $scope.verificarListaCheckbox = function (id) {
      for(x in $scope.listaCracha){
        if($scope.listaCracha[x].id == id){
          return true;
        }
      }
      return false;
    };

    $scope.listaCheckbox = function (obj) {
      var add = true;
      for(x in $scope.listaCracha){
        if($scope.listaCracha[x].id == obj.id){
          $scope.listaCracha.splice(x, 1);
          add = false;
        }
      }
      if(add){
        $scope.listaCracha.push(obj);
      }
    };

    $scope.gerarCrachas = function () {
      if($scope.listaCracha.length == 0){
        swal("Selecione ao menos um usuário!", "", "error");
      }else{
        var text = 'Código, Função, Nome Completo, CPF, Adicional 01\n';
        for(x in $scope.listaCracha){
          text += $scope.listaCracha[x].id + ', ';
          text += $scope.listaCracha[x].tipo + ', ';
          text += $scope.listaCracha[x].nome + ', ';
          text += $scope.listaCracha[x].cpf + ', ';
          text += '{"id": ' + $scope.listaCracha[x].id + ', "nome": "' + $scope.listaCracha[x].nome + '", "cpf": "' + $scope.listaCracha[x].cpf + '"}\n';
        }
        var blob = new Blob([text], {
              		type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8"
          		});
        saveAs(blob, "Crachás Usuários.xls");
        $scope.listaCracha = [];
        for(x in $scope.checkboxes){
          $scope.checkboxes[x] = false;
        }
      }
    };

    $scope.mostrarEsconderVinculados = function (mostrarVinculados) {
      if(!mostrarVinculados){
        $scope.usuarios = $scope.bkp;
      }else{
        var novo = [];
        for(x in $scope.usuarios){
          if($scope.usuarios[x].id_projeto == $rootScope.global.projeto.id){
            novo.push($scope.usuarios[x]);
          }
        }
        $scope.usuarios = novo;
      }
    };

    $scope.adicionarUsuario = function () {
      if($scope.validarDados()){
        if(!angular.isUndefined($rootScope.global.projeto)){
          $scope.usuario.id_projeto = $rootScope.global.projeto.id;
        }else{
          $scope.usuario.id_projeto = "";
        }
        $scope.usuario.usuario_sessao = $rootScope.global.usuario.id;
        Usuario.cadastrar($scope.usuario)
         .success(function(response) {
           if(response != null) {
             if(response.status === 2){
               swal('Login já cadastrado!', '', 'warning');
               $scope.usuario.senha = "";
             }else if(response.status === 3){
               swal('CPF já cadastrado!', '', 'warning');
               $scope.usuario.senha = "";
             }else{
               swal({
                   title: "Usuário cadastrado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarUsuarios');
                 }
               );
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar usuário!');
         });
      }
    };

    $scope.editarUsuario = function () {
      if($scope.validarDados()){
        $scope.usuario.usuario_sessao = $rootScope.global.usuario.id;
        Usuario.editar($scope.usuario)
         .success(function(response) {
           if(response != null) {
             if(response.status === 2){
               swal('Login já cadastrado!', '', 'warning');
               $scope.usuario.senha = "";
             }else if(response.status === 3){
               swal('CPF já cadastrado!', '', 'warning');
               $scope.usuario.senha = "";
             }else{
               swal({
                   title: "Usuário editado com sucesso!", type:"success"
                 },
                 function(){
                   $state.go('listarUsuarios');
                 }
               );
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar usuário!');
         });
      }
    };

    $scope.excluirUsuario = function (id) {
      swal({
              title: "Você tem certeza?",
              text: "Esse usuário será excluído PERMANENTEMENTE!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                var obj = {id: id, usuario_sessao: $rootScope.global.usuario.id}
                Usuario.excluir(id)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       swal({
                           title: "Usuário excluído com sucesso!", type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir esse usuário!", 'Esse usuário está vinculado a um veículo. Desvincule antes de excluir', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir usuário!');
                 });
        });
    };

    $scope.vincularUsuario = function (usuario) {
      if(usuario.id_projeto == "" || usuario.id_projeto == null || angular.isUndefined(usuario.id_projeto) || usuario.id_projeto == 0){
        Usuario.vincularProjeto({id_pessoa: usuario.id, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
         .success(function(response) {
           if(response != null) {
             if(response.status){
               swal('Vinculado com sucesso!', '', 'success');
               $state.go($state.current, {}, {reload: true});
             }else{
               swal('Erro ao vincular usuário', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao vincular usuário!');
         });
       }else{
         swal('Usuário já vinculado!', 'Esse usuário já está vinculado à outro projeto! Favor, desvincular antes de tentar vincular à esse projeto.', 'warning');
       }
    };

    $scope.desvincularUsuario = function (id_usuario) {
      Usuario.desvincularProjeto({id_pessoa: id_usuario, id_projeto: $rootScope.global.projeto.id, usuario_sessao: $rootScope.global.usuario.id})
       .success(function(response) {
         if(response != null) {
           if(response.status){
             swal('Desvinculado com sucesso!', '', 'success');
             $state.go($state.current, {}, {reload: true});
           }else{
             swal('Não foi possível desvincular usuário!', 'Esse usuário está vinculado à um veículo', 'warning');
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao vincular veículo!');
       });
    };

    $scope.gerarQRCodeUsuario = function (u) {
      var obj = {qrcode: "{id: '" + u.id + "', login: '" + u.login + "', cpf: '" + u.cpf + "'}", usuario: u};
      Usuario.baixarQRCode(obj)
       .success(function(data) {
          var blob = $scope.b64toBlob(data.imagem);
          var blobUrl = URL.createObjectURL(blob);
          var anchor = document.createElement('a');
          anchor.href = blobUrl;
          anchor.target = '_blank';
          anchor.download = 'Crachá usuário ID ' + u.id;
          anchor.click();
       }).error(function(erro) {
         swal('Erro ao gerar QRCode!');
       });
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if(angular.isUndefined($scope.usuario.id_tipo_pessoa)){
        idn = 'id_tipo_pessoa';
      }else{
        var novoTelefone = $scope.usuario.telefone.match(/\d/g);
        novoTelefone = novoTelefone.join("");
        if(angular.isUndefined($scope.usuario.nome) || $scope.usuario.nome == ""){
          idn = 'nome';
        }else if(angular.isUndefined($scope.usuario.telefone) || novoTelefone == ""){
          idn = 'telefone';
        }else if(angular.isUndefined($scope.usuario.endereco) || $scope.usuario.endereco == ""){
          idn = 'endereco';
        }else if(angular.isUndefined($scope.usuario.email) || $scope.usuario.email == ""){
          idn = 'email';
        }else if(angular.isUndefined($scope.usuario.login) || $scope.usuario.login == ""){
          idn = 'login';
        }else if(angular.isUndefined($scope.usuario.cpf) || $scope.usuario.cpf == "" || $scope.formulario.cpf.$invalid){
          idn = 'cpf';
        }else if(angular.isUndefined($scope.usuario.senha) || $scope.usuario.senha == ""){
          if($scope.usuario.id_tipo_pessoa == 2){
            if(!$scope.editar){
              idn = 'senha';
            }else{
              status = 1;
            }
          }else{
            var cpf3 = $scope.usuario.cpf.match(/\d/g);
            cpf3 = cpf3.join("");
            cpf3 = cpf3.substring(0,3);
            $scope.usuario.senha = $scope.usuario.login + cpf3;
            status = 1;
          }
        }else if(angular.isUndefined($scope.usuario.csenha) || $scope.usuario.csenha == ""){
          if($scope.usuario.id_tipo_pessoa == 2){
            if(!$scope.editar){
              idn = 'csenha';
            }else{
              status = 1;
            }
          }
        }else if($scope.usuario.senha != $scope.usuario.csenha){
          swal('O campo "Confirmar senha" deve ser igual ao campo "Senha"', '', 'warning');
          idn = 'csenha';
        }else{
          status = 1;
        }
      }
      if(status == 0){
        if(idn == 'id_tipo_pessoa'){
          swal('O campo "Perfil de Acesso" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    $scope.listarUsuarios = function () {
      // Usuario.listar($rootScope.global.projeto.id)
      Usuario.listar()
       .success(function(data) {
         if(data.status){
           $scope.usuarios = data.usuarios;
           $scope.bkp = data.usuarios;
         }else{
          swal('Nenhum usuário encontrado!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar usuários!');
       });
    };

    $scope.listarTiposUsuario = function () {
      Usuario.listarTipo()
       .success(function(data) {
         if(data.status){
           if($rootScope.global.usuario.id_tipo_pessoa == 55){
             $scope.tipo_pessoa = [];
             for(x in data.tipos){
               if(data.tipos[x].descricao == "Motorista" || data.tipos[x].descricao == "Operador" || data.tipos[x].descricao == "Admin. Obra"){
                 $scope.tipo_pessoa.push(data.tipos[x]);
               }
             }
          }else{
            $scope.tipo_pessoa = data.tipos;
          }
         }else{
          swal('Nenhum tipo de usuário encontrado!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar tipos de usuário!');
       });
    };

    $scope.buscarProximoIdUsuario = function () {
      Usuario.buscarProximoId()
       .success(function(data) {
         if(data.status){
           $scope.usuario.id = data.id;
         }else{
          swal('Não foi possível recuperar id do usuário!');
         }
       }).error(function(erro) {
         swal('Erro ao buscar id do projeto!');
       });
    };

    $scope.b64toBlob = function (b64Data, sliceSize) {
      contentType = 'image/png';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    };

    (function initController() {
      if($location.path() === '/usuarios/listar'){
        $scope.listarUsuarios();
      }else if($location.path() === '/usuario/adicionar'){
        $scope.usuario = {};
        $scope.buscarProximoIdUsuario();
        $scope.listarTiposUsuario();
      }else if($location.path() === '/usuario/editar'){
        $scope.editar = 1;
        $scope.listarTiposUsuario();
        Usuario.buscar($stateParams.id)
         .success(function(data) {
           if(data.status){
             $scope.usuario = data.usuario;
             if($scope.usuario.telefone.length == 14){
               $scope.phoneMask = "(99) 9999-9999";
             }else{
               $scope.phoneMask = "(99) 99999-9999";
             }
           }else{
             swal({
                 title: "Usuário não encontrado!"
               },
               function(){
                 $state.go('listarUsuarios');
               }
             );
           }
         }).error(function(erro) {
           swal({
               title: "Erro ao buscar usuário!"
             },
             function(){
               $state.go('listarUsuarios');
             }
           );
         });
      }
    })();

});
