app.controller('ConfiguracoesController', function($stateParams, $rootScope, $location, $scope, $state, Tipos){

    $scope.phoneMask = "(99) 9999-9999";
    $scope.acao = function () {
      if($location.path().indexOf('nova') !== -1){
        $scope.adicionarTipoGenerico();
      }else{
        $scope.editarTipoGenerico();
      }
    };

    $scope.adicionarTipoGenerico = function () {
      if($scope.validarDados()){
        var obj = {tipo: $scope.tipo, tipo_atual: $scope.tipo_atual, usuario_sessao: $rootScope.global.usuario.id};
        Tipos.cadastrar(obj)
         .success(function(response) {
           if(response != null) {
             if(response.status){
                var msg = "Tipo cadastrado com sucesso!";
                if($scope.tipo_atual == 'terceiros'){
                  msg = "Terceiro cadastrado com sucesso!";
                }
                swal({
                    title: msg, type:"success"
                  },
                  function(){
                    $state.go('listarTiposGenerico', {tipo: $scope.tipo_atual});
                  }
                );
             }else{
               if($scope.tipo_atual == 'tipo_inatividade'){
                  swal('Erro ao cadastrar tipo de inatividade!', 'Não existe nenhum tipo de atividade com descrição igual à "Inatividade"', 'error');
               }else{
                 swal('Erro ao cadastrar!', '', 'error');
              }
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao cadastrar!');
         });
      }
    };

    $scope.editarTipoGenerico = function () {
      if($scope.validarDados()){
        var obj = {tipo: $scope.tipo, tipo_atual: $scope.tipo_atual, usuario_sessao: $rootScope.global.usuario.id};
        Tipos.editar(obj)
         .success(function(response) {
           if(response != null) {
             if(response.status){
               var msg = "Tipo editado com sucesso!";
               if($scope.tipo_atual == 'terceiros'){
                 msg = "Terceiro editado com sucesso!";
               }
               swal({
                   title: msg, type:"success"
                 },
                 function(){
                   $state.go('listarTiposGenerico', {tipo: $scope.tipo_atual});
                 }
               );
             }else{
               swal('Erro ao editar!', '', 'error');
             }
           } else {
             swal("ERRO");
           }
         }).error(function(erro) {
           swal('Erro ao editar!');
         });
      }
    };

    $scope.excluirTipoGenerico = function (id, tipo) {
      var obj = {id: id, tipo: tipo, usuario_sessao: $rootScope.global.usuario.id};
      var msg = "Esse tipo será excluído PERMANENTEMENTE!";
      if($scope.tipo_atual == 'terceiros'){
        msg = "Esse terceiro será excluído PERMANENTEMENTE!";
      }
      swal({
              title: "Você tem certeza?",
              text: msg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Confirmar",
              closeOnConfirm: false },
              function(){
                Tipos.excluir(obj)
                 .success(function(response) {
                   if(response != null) {
                     if(response.status){
                       var msg = "Tipo excluído com sucesso!";
                       if($scope.tipo_atual == 'terceiros'){
                         msg = "Terceiro excluído com sucesso!";
                       }
                       swal({
                           title: msg, type:"success"
                         },
                         function(){
                           $state.go($state.current, {}, {reload: true});
                         }
                       );
                     }else{
                       swal("Não foi possível excluir esse tipo!", 'O tipo atividade "Inatividade" está vinculado aos tipos inatividades.', 'error');
                     }
                   } else {
                     swal("ERRO");
                   }
                 }).error(function(erro) {
                   swal('Erro ao excluir!');
                 });
        });
    };

    $scope.buscarTipo = function () {
      var obj = {id: $stateParams.id, tipo_atual: $scope.tipo_atual};
      Tipos.buscar(obj)
       .success(function(response) {
         if(response != null) {
           if(response.status){
             $scope.tipo = response.tipo;
             if($scope.tipo.complemento != '' && $scope.tipo.complemento != null){
               $scope.tipo.complemento = true;
             }
             if($scope.tipo_atual == 'terceiros'){
               if($scope.tipo.telefone.length == 14){
                 $scope.phoneMask = "(99) 9999-9999";
               }else{
                 $scope.phoneMask = "(99) 99999-9999";
               }
             }
           }else{
             swal({
                 title: "Não encontrado!"
               },
               function(){
                 $state.go('listarTiposGenerico', {tipo: $scope.tipo_atual});
               }
             );
           }
         } else {
           swal("ERRO");
         }
       }).error(function(erro) {
         swal('Erro ao buscar!');
       });
    };

    $scope.validarDados = function () {
      var status = 0;
      var campo = "";
      var idn = "";
      if($scope.tipo_atual == 'terceiros'){
        var novoTelefone = $scope.tipo.telefone.match(/\d/g);
        novoTelefone = novoTelefone.join("");
      }
      if($scope.tipo_atual == 'tipo_pessoa' && angular.isUndefined($scope.tipo.acesso)){
        idn = 'acesso';
      }else if($scope.tipo_atual != 'terceiros' && (angular.isUndefined($scope.tipo.descricao) || $scope.tipo.descricao == "")){
        idn = 'descricao';
      }else if($scope.tipo_atual != 'terceiros' && $scope.complemento_manual && (angular.isUndefined($scope.tipo.complemento) || $scope.tipo.complemento == "") && $scope.mcm){
        idn = 'cmanual';
      }else if($scope.tipo_atual == 'terceiros' &&(angular.isUndefined($scope.tipo.nome) || $scope.tipo.nome == "")){
        idn = 'nome';
      }else if($scope.tipo_atual == 'terceiros' && (angular.isUndefined($scope.tipo.telefone) || novoTelefone == "")){
        idn = 'telefone';
      }else if($scope.tipo_atual == 'terceiros' &&(angular.isUndefined($scope.tipo.email) || $scope.tipo.email == "")){
        idn = 'email';
      }else{
        status = 1;
      }
      if(status == 0){
        if(idn == 'tipo_atividade'){
          swal('O campo "Tipo Atividade" deve ser preenchido', '', 'warning');
        }else if(idn == 'acesso'){
          swal('O campo "Acesso" deve ser preenchido', '', 'warning');
        }else{
          document.getElementById(idn).focus();
        }
        return false;
      }else{
        return true;
      }
    };

    $scope.verificarTipoAtual = function () {
      $scope.mcm = false;
      if($stateParams.tipo == 'tipo_atividade'){
          $scope.configuracaoAtual = "Tipo Atividade";
          $scope.mcm = true;
      }else if($stateParams.tipo == 'tipo_camada'){
          $scope.configuracaoAtual = "Tipo Camada";
      }else if($stateParams.tipo == 'tipo_inatividade'){
          $scope.configuracaoAtual = "Tipo Inatividade";
          $scope.mcm = true;
      }else if($stateParams.tipo == 'tipo_local'){
          $scope.configuracaoAtual = "Tipo Local";
      }else if($stateParams.tipo == 'tipo_manutencao'){
          $scope.configuracaoAtual = "Tipo Manutenção";
          $scope.mcm = true;
      }else if($stateParams.tipo == 'tipo_origem'){
          $scope.configuracaoAtual = "Tipo Origem";
      }else if($stateParams.tipo == 'tipo_pessoa'){
          $scope.configuracaoAtual = "Perfil Acesso";
      }else if($stateParams.tipo == 'tipo_veiculo'){
          $scope.configuracaoAtual = "Tipo Veículo";
      }else if($stateParams.tipo == 'unidade'){
          $scope.configuracaoAtual = "Unidade";
      }else if($stateParams.tipo == 'terceiros'){
          $scope.configuracaoAtual = "Terceiros";
      }
      $scope.tipo_atual = $stateParams.tipo;
    };

    $scope.buscarTiposGenerico = function () {
        var enviar = {tipo: $stateParams.tipo};
        Tipos.listar(enviar)
         .success(function(data) {
           if(data.status){
             $scope.tipos = data.tipos;
           }else{
             if($scope.tipo_atual == "terceiros"){
               swal('Nenhum terceiro cadastrado!', '', 'warning');
             }else{
               swal('Nenhum tipo cadastrado!', '', 'warning');
            }
           }
         }).error(function(erro) {
           swal('Erro ao buscar!');
         });
    };

    $scope.buscarTiposAtividade = function () {
        var enviar = {tipo: 'tipo_atividade'};
        Tipos.listar(enviar)
         .success(function(data) {
           if(data.status){
             $scope.tipo_atividade = data.tipos;
           }else{
             swal({
                 title: "Nenhum tipo de atividade cadastrado!", type:"error"
               },
               function(){
                 $state.go('listarTiposGenerico', {tipo: 'tipo_inatividade'});
               }
             );
           }
         }).error(function(erro) {
           swal('Erro ao buscar tipos de atividade!');
         });
    };

    $scope.iniciarTipos = function () {
      $scope.verificarTipoAtual();
      if($stateParams.tipo == 'tipo_inatividade'){
        $scope.buscarTiposAtividade();
      }
      if($location.path().indexOf('listar') !== -1){
        $scope.buscarTiposGenerico();
      }else if($location.path().indexOf('editar') !== -1){
        $scope.buscarTipo();
      }
    };

    (function initController() {
      $rootScope.aparecerConfiguracoes = true;
      $scope.tipo = {};
      $scope.iniciarTipos();
    })();

});
