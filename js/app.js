var app = angular.module('app', [ 'ui.router', 'ngAnimate', 'ui.materialize', 'ngResource', 'angularUtils.directives.dirPagination', 'ngCookies', 'ui.mask', 'ngCpfCnpj', 'ng-file-model', 'ui.bootstrap', 'ngMap']);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider) {


// $urlRouterProvider.otherwise('login');

	$stateProvider.state('login', {
		url: "/login",
		templateUrl: "pages/login.html",
		controller: "LoginController"
	})

	.state('eas', {
		url: "/esqueceu_a_senha",
		templateUrl: "pages/login.html",
		controller: "LoginController"
	})

	.state('logout', {
		url: "/logout",
		controller: "LogoutController"
	})

	.state('esqueceu_a_senha', {
		url: "/esqueceu_a_senha/:hash",
		templateUrl: "pages/esqueceu_a_senha.html",
		controller: "EsqueceuSenhaController"
	})

	.state('home', {
		url: "home",
		templateUrl: "pages/home.html",
		controller: "HomeController"
	})

	.state('listarProjetos', {
		url: "projetos/listar",
		templateUrl: "pages/projeto/listar.html",
		controller: "ProjetoController"
	})

	.state('adicionarProjeto', {
		url: "projetos/novo",
		templateUrl: "pages/projeto/formulario.html",
		controller: "ProjetoController"
	})

	.state('editarProjeto', {
		url: "projetos/editar",
		templateUrl: "pages/projeto/formulario.html",
		controller: "ProjetoController",
		params : {id: null, editar:1}
	})

	.state('listarVeiculos', {
		url: "veiculos/listar",
		templateUrl: "pages/veiculo/listar.html",
		controller: "VeiculoController"
	})

	.state('adicionarVeiculo', {
		url: "veiculos/novo",
		templateUrl: "pages/veiculo/formulario.html",
		controller: "VeiculoController",
		params: {veiculo: null}
	})

	.state('editarVeiculo', {
		url: "veiculos/editar",
		templateUrl: "pages/veiculo/formulario.html",
		controller: "VeiculoController",
		params : {id: null}
	})

	.state('listarUsuarios', {
		url: "usuarios/listar",
		templateUrl: "pages/usuario/listar.html",
		controller: "UsuarioController"
	})

	.state('adicionarUsuario', {
		url: "usuario/adicionar",
		templateUrl: "pages/usuario/formulario.html",
		controller: "UsuarioController"
	})

	.state('editarUsuario', {
		url: "usuario/editar",
		templateUrl: "pages/usuario/formulario.html",
		controller: "UsuarioController",
		params : {id: null}
	})

	.state('listarLocais', {
		url: "locais/listar",
		templateUrl: "pages/local/listar.html",
		controller: "LocalController"
	})

	.state('adicionarLocal', {
		url: "locais/novo",
		templateUrl: "pages/local/formulario.html",
		controller: "LocalController as vm",
		params: {local: null}
	})

	.state('editarLocal', {
		url: "locais/editar",
		templateUrl: "pages/local/formulario.html",
		controller: "LocalController as vm",
		params : {id: null}
	})

	.state('importarEstacas', {
		url: "locais/estacas/importar",
		templateUrl: "pages/local/estacas.html",
		controller: "LocalController"
	})

	.state('listarOrigens', {
		url: "origens/listar",
		templateUrl: "pages/origem/listar.html",
		controller: "OrigemController"
	})

	.state('adicionarOrigem', {
		url: "origens/nova",
		templateUrl: "pages/origem/formulario.html",
		controller: "OrigemController",
		params: {origem: null}
	})

	.state('editarOrigem', {
		url: "origens/editar",
		templateUrl: "pages/origem/formulario.html",
		controller: "OrigemController",
		params : {id: null}
	})

	.state('configuracoes', {
		url: "configuracoes",
		templateUrl: "pages/configuracoes/home.html",
		controller: "ConfiguracoesController"
	})

	.state('listarTiposGenerico', {
		url: "configuracoes/listar",
		templateUrl: "pages/configuracoes/listar.html",
		controller: "ConfiguracoesController",
		params : {tipo: null}
	})

	.state('adicionarTipoGenerico', {
		url: "configuracoes/nova",
		templateUrl: "pages/configuracoes/formulario.html",
		controller: "ConfiguracoesController",
		params : {tipo: null}
	})

	.state('editarTipoGenerico', {
		url: "configuracoes/editar",
		templateUrl: "pages/configuracoes/formulario.html",
		controller: "ConfiguracoesController",
		params : {id: null, tipo: null}
	})

	.state('listarOpV', {
		url: "operador-x-veiculo/listar",
		templateUrl: "pages/operador-x-veiculo/listar.html",
		controller: "OperadorVeiculoController"
	})

	.state('listarOpG', {
		url: "operador-x-origem/listar",
		templateUrl: "pages/operador-x-origem/listar.html",
		controller: "OperadorOrigemController"
	})

	.state('listarCamadas', {
		url: "camadas/listar",
		templateUrl: "pages/camada/listar.html",
		controller: "CamadaController"
	})

	.state('adicionarCamada', {
		url: "camadas/nova",
		templateUrl: "pages/camada/formulario.html",
		controller: "CamadaController",
		params: {camada: null}
	})

	.state('editarCamada', {
		url: "camadas/editar",
		templateUrl: "pages/camada/formulario.html",
		controller: "CamadaController",
		params : {id: null}
	})

	.state('listarMateriais', {
		url: "materiais/listar",
		templateUrl: "pages/material/listar.html",
		controller: "MaterialController"
	})

	.state('adicionarMaterial', {
		url: "materiais/novo",
		templateUrl: "pages/material/formulario.html",
		controller: "MaterialController",
		params: {material: null}
	})

	.state('editarMaterial', {
		url: "materiais/editar",
		templateUrl: "pages/material/formulario.html",
		controller: "MaterialController",
		params : {id: null}
	})

	.state('logs', {
		url: "log",
		templateUrl: "pages/log/listar.html",
		controller: "LogController"
	})

});

app.run(function($cookies, $rootScope, $location, $state, $cookies, $timeout) {
		$rootScope.baseUrl = window.location.origin + window.location.pathname;
		// keep user logged in after page refresh
		$rootScope.global = $cookies.get('global') || {};
		$rootScope.global.projeto = "";
		$rootScope.$on('$locationChangeStart', function(event, next, current) {
		if($location.path().indexOf('configuracoes') === -1){
			$rootScope.aparecerConfiguracoes = false;
		}
		// redirect to login page if not logged in
		if ($location.path() !== '/login' && angular.isUndefined($rootScope.global.usuario)	&& $location.path() !== '/logout' && $location.path() !== '/esqueceu_a_senha') {
					$state.go('login');
		}
	});
});


app.directive('capitalize', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, modelCtrl) {
      var capitalize = function(inputValue) {
        if (inputValue == undefined) inputValue = '';
        var capitalized = inputValue.toUpperCase();
        if (capitalized !== inputValue) {
          modelCtrl.$setViewValue(capitalized);
          modelCtrl.$render();
        }
        return capitalized;
      }
      modelCtrl.$parsers.push(capitalize);
      capitalize(scope[attrs.ngModel]); // capitalize initial value
    }
  };
});
app.directive('maskChange', function() {
    return {
        restrict: 'A',
        scope: {
            maskChange: "=",
        },
        require: '?ngModel',
        link: function(scope, elem, attrs, ngModel) {

            var novoTel, flag = false, val;

            elem.off('keyup');
            elem.on('keyup', function(ev) {

                if (/^\d+$/.test(ev.key) || ev.key == 'Backspace' || ev.key == 'Delete') {

                    novoTel = String(ngModel.$viewValue).replace(/[\(\)\_\-/\s]/g, '')
                    if (novoTel.length == 10 && !flag) {
												var bkp = ngModel.$viewValue += ev.key;
                        flag = true;
                        scope.maskChange = "(99) 9999-9999";
                        scope.$apply();
                        ngModel.$viewValue = bkp;
                        ngModel.$render();
                    } else if (novoTel.length == 10 && flag) {
												var bkp = ngModel.$viewValue += ev.key;
                        flag = false;
                        scope.maskChange = "(99) 9?9999-9999";
                        scope.$apply();
                        ngModel.$viewValue = bkp;
                        ngModel.$render();

                    } else if (novoTel.length < 10) {
                        flag = false;
                    }
                }
            })
        }

    };
});
