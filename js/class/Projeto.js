angular.module("app").factory(
		"Projeto",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(projeto) {
				return $http.post($rootScope.baseUrl + "php/projeto/cadastrar.php", projeto);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/projeto/excluir.php",	id);
			}

			var _MetodoEditar = function(projeto) {
				return $http.post($rootScope.baseUrl + "php/projeto/editar.php", projeto);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/projeto/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/projeto/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(){
				return $http.post($rootScope.baseUrl + "php/projeto/listar.php", 'oi');
			}

			var _MetodoBuscarProximoId = function(){
				return $http.post($rootScope.baseUrl + "php/projeto/buscar_proximo_id.php");
			}

			var _MetodoListarPorUsuario = function(id){
				return $http.post($rootScope.baseUrl + "php/projeto/buscar_por_usuario.php", id);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar,
				buscarProximoId : _MetodoBuscarProximoId,
				listarPorUsuario : _MetodoListarPorUsuario
			}
		});
