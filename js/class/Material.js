angular.module("app").factory(
		"Material",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/material/cadastrar.php", obj);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/material/excluir.php",	id);
			}

			var _MetodoEditar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/material/editar.php", obj);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/material/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/material/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(obj){
				return $http.post($rootScope.baseUrl + "php/material/listar.php", obj);
			}

			var _MetodoListarUnidades = function(){
				return $http.post($rootScope.baseUrl + "php/material/listar_unidades.php", 'oi');
			}

			var _MetodoVincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/material/vincular_projeto.php", obj);
			}

			var _MetodoDesvincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/material/desvincular_projeto.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar,
				listarUnidades: _MetodoListarUnidades,
				vincularProjeto : _MetodoVincularProjeto,
				desvincularProjeto : _MetodoDesvincularProjeto
			}
		});
