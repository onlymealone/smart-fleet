angular.module("app").factory(
		"Origem",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(origem) {
				return $http.post($rootScope.baseUrl + "php/origem/cadastrar.php", origem);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/origem/excluir.php",	id);
			}

			var _MetodoEditar = function(origem) {
				return $http.post($rootScope.baseUrl + "php/origem/editar.php", origem);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/origem/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/origem/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(id){
				return $http.post($rootScope.baseUrl + "php/origem/listar.php", id);
			}

			var _MetodoListarTipos = function(){
				return $http.post($rootScope.baseUrl + "php/origem/listar_tipos.php", 'oi');
			}

			var _MetodoListarOrigens = function(obj){
				return $http.post($rootScope.baseUrl + "php/origem/listar_origens.php", obj);
			}

			var _MetodoVincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/origem/vincular_projeto.php", obj);
			}

			var _MetodoDesvincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/origem/desvincular_projeto.php", obj);
			}

			var _MetodoBaixarQRCode = function(obj){
				return $http.post($rootScope.baseUrl + "php/origem/gerar_qrcode.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar,
				listarTipos: _MetodoListarTipos,
				listarOrigens: _MetodoListarOrigens,
				vincularProjeto : _MetodoVincularProjeto,
				desvincularProjeto : _MetodoDesvincularProjeto,
				baixarQRCode: _MetodoBaixarQRCode,
			}
		});
