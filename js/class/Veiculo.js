angular.module("app").factory(
		"Veiculo",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(veiculo) {
				return $http.post($rootScope.baseUrl + "php/veiculo/cadastrar.php", veiculo);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/veiculo/excluir.php",	id);
			}

			var _MetodoEditar = function(veiculo) {
				return $http.post($rootScope.baseUrl + "php/veiculo/editar.php", veiculo);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/veiculo/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/veiculo/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(){
			// var _MetodoListar = function(id){
			// 	return $http.post($rootScope.baseUrl + "php/veiculo/listar.php", id);
				return $http.post($rootScope.baseUrl + "php/veiculo/listar.php");
			}

			var _MetodoBaixarQRCode = function(obj){
				return $http.post($rootScope.baseUrl + "php/veiculo/gerar_qrcode.php", obj);
			}

			var _MetodoListarTipos = function(){
				return $http.post($rootScope.baseUrl + "php/veiculo/listar_tipos.php", 'oi');
			}

			var _MetodoListarVeiculos = function(obj){
				return $http.post($rootScope.baseUrl + "php/veiculo/listar_veiculos.php", obj);
			}

			var _MetodoDesvincular = function(obj){
				return $http.post($rootScope.baseUrl + "php/veiculo/desvincular.php", obj);
			}

			var _MetodoVincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/veiculo/vincular_projeto.php", obj);
			}

			var _MetodoDesvincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/veiculo/desvincular_projeto.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				baixarQRCode: _MetodoBaixarQRCode,
				ativarDesativar : _MetodoAtivarDesativar,
				listarTipos: _MetodoListarTipos,
				listarVeiculos: _MetodoListarVeiculos,
				desvincular : _MetodoDesvincular,
				vincularProjeto : _MetodoVincularProjeto,
				desvincularProjeto : _MetodoDesvincularProjeto
			}
		});
