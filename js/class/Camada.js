angular.module("app").factory(
		"Camada",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/camada/cadastrar.php", obj);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/camada/excluir.php",	id);
			}

			var _MetodoEditar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/camada/editar.php", obj);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/camada/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/camada/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(obj){
				return $http.post($rootScope.baseUrl + "php/camada/listar.php", obj);
			}

			var _MetodoListarTipos = function(){
				return $http.post($rootScope.baseUrl + "php/camada/listar_tipos.php", 'oi');
			}

			var _MetodoVincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/camada/vincular_projeto.php", obj);
			}

			var _MetodoDesvincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/camada/desvincular_projeto.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar,
				listarTipos: _MetodoListarTipos,
				vincularProjeto : _MetodoVincularProjeto,
				desvincularProjeto : _MetodoDesvincularProjeto
			}
		});
