angular.module("app").factory(
		"Terceiro",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/terceiro/cadastrar.php", obj);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/terceiro/excluir.php",	id);
			}

			var _MetodoEditar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/terceiro/editar.php", obj);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/terceiro/buscar.php", id);
			}

			var _MetodoListar = function(){
				return $http.post($rootScope.baseUrl + "php/terceiro/listar.php", 'oi');
			}

			var _MetodoBuscarProximoId = function(){
				return $http.post($rootScope.baseUrl + "php/terceiro/buscar_proximo_id.php");
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				buscarProximoId : _MetodoBuscarProximoId
			}
		});
