angular.module("app").factory(
		"Tipos",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/tipos/cadastrar.php", obj);
			}

			var _MetodoExcluir = function(obj) {
				return $http.post($rootScope.baseUrl + "php/tipos/excluir.php",	obj);
			}

			var _MetodoEditar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/tipos/editar.php", obj);
			}

			var _MetodoBuscar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/tipos/buscar.php", obj);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/tipos/ativar_desativar.php", obj);
			}

			var _MetodoListar = function(tipo){
				return $http.post($rootScope.baseUrl + "php/tipos/listar.php", tipo);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar
			}
		});
