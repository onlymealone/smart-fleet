angular.module("app").factory(
		"Local",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(local) {
				return $http.post($rootScope.baseUrl + "php/local/cadastrar.php", local);
			}

			var _MetodoCadastrarEstacas = function(obj) {
				return $http.post($rootScope.baseUrl + "php/local/cadastrar_estacas.php", obj);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/local/excluir.php",	id);
			}

			var _MetodoEditar = function(local) {
				return $http.post($rootScope.baseUrl + "php/local/editar.php", local);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/local/buscar.php", id);
			}

			var _MetodoListar = function(id){
				return $http.post($rootScope.baseUrl + "php/local/listar.php", id);
			}

			var _MetodoListarTipos = function(){
				return $http.post($rootScope.baseUrl + "php/local/listar_tipos.php", 'oi');
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				listarTipos: _MetodoListarTipos,
				cadastrarEstacas: _MetodoCadastrarEstacas
			}
		});
