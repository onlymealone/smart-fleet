angular.module("app").factory(
		"AuthService",
		function(Base64, $http, $cookies, $rootScope, $timeout, Usuario, $state) {
			var service = {}

			service.Login = function(usuario, callback) {
				 Usuario.logar(usuario)
					.success(function(data) {
						callback(data);
					}).error(function(erro) {
						// $state.go('erro');
					});
			};

			service.AtualizarCredenciais = function(usuario) {
				// console.log("atualizando credenciais");
				var key = Base64.encode(usuario.login + ':' + usuario.senha);
				$rootScope.global = {
					usuario : {
						login : usuario.login,
						nome: usuario.nome,
						key : key,
						id : usuario.id,
						id_tipo_pessoa : usuario.id_tipo_pessoa,
						id_projeto : usuario.id_projeto
					}
				};
				// console.log($rootScope.global);
				$cookies.put('global', $rootScope.global);
				// console.log('setou cookies');
			};

			service.LimparCredenciais = function() {
				// console.log("limpando credenciais");
				$rootScope.global = {};
				$cookies.remove('global');
			};
			return service;
		})
angular
		.module("app")
		.factory(
				'Base64',
				function() {
					var keyStr = 'dnis87a8==DH=A//Sy+9UDd86=atacGAY+/d13829r0iHGSA=+/SD98';
					return {
						encode : function(input) {
							var output = "";
							var chr1, chr2, chr3 = "";
							var enc1, enc2, enc3, enc4 = "";
							var i = 0;
							do {
								chr1 = input.charCodeAt(i++);
								chr2 = input.charCodeAt(i++);
								chr3 = input.charCodeAt(i++);
								enc1 = chr1 >> 2;
								enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
								enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
								enc4 = chr3 & 63;
								if (isNaN(chr2)) {
									enc3 = enc4 = 64;
								} else if (isNaN(chr3)) {
									enc4 = 64;
								}
								output = output + keyStr.charAt(enc1)
										+ keyStr.charAt(enc2)
										+ keyStr.charAt(enc3)
										+ keyStr.charAt(enc4);
								chr1 = chr2 = chr3 = "";
								enc1 = enc2 = enc3 = enc4 = "";
							} while (i < input.length);
							return output;
						},
						decode : function(input) {
							var output = "";
							var chr1, chr2, chr3 = "";
							var enc1, enc2, enc3, enc4 = "";
							var i = 0;
							var base64test = /[^A-Za-z0-9\+\/\=]/g;
							if (base64test.exec(input)) {
								window
										.alert("Você inseriu caracteres inválidos.\n"
												+ "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n"
												+ "Expect errors in decoding.");
							}
							input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
							do {
								enc1 = keyStr.indexOf(input.charAt(i++));
								enc2 = keyStr.indexOf(input.charAt(i++));
								enc3 = keyStr.indexOf(input.charAt(i++));
								enc4 = keyStr.indexOf(input.charAt(i++));
								chr1 = (enc1 << 2) | (enc2 >> 4);
								chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
								chr3 = ((enc3 & 3) << 6) | enc4;
								output = output + String.fromCharCode(chr1);
								if (enc3 != 64) {
									output = output + String.fromCharCode(chr2);
								}
								if (enc4 != 64) {
									output = output + String.fromCharCode(chr3);
								}
								chr1 = chr2 = chr3 = "";
								enc1 = enc2 = enc3 = enc4 = "";
							} while (i < input.length);
							return output;
						}
					};
				});
