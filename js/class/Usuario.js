angular.module("app").factory(
		"Usuario",
		function($http, $rootScope) {

			var _MetodoCadastrar = function(usuario) {
				return $http.post($rootScope.baseUrl + "php/usuario/cadastrar.php", usuario);
			}

			var _MetodoExcluir = function(id) {
				return $http.post($rootScope.baseUrl + "php/usuario/excluir.php",	id);
			}

			var _MetodoEditar = function(usuario) {
				return $http.post($rootScope.baseUrl + "php/usuario/editar.php", usuario);
			}

			var _MetodoBuscar = function(id) {
				return $http.post($rootScope.baseUrl + "php/usuario/buscar.php", id);
			}

			var _MetodoAtivarDesativar = function(obj) {
				return $http.post($rootScope.baseUrl + "php/usuario/ativar_desativar.php", obj);
			}

			var _MetodoLogar = function(usuario) {
				return $http.post($rootScope.baseUrl + "php/usuario/login.php", usuario);
			}

			var _MetodoListar = function(){
			// var _MetodoListar = function(id){
			// 	return $http.post($rootScope.baseUrl + "php/usuario/listar.php", id);
				return $http.post($rootScope.baseUrl + "php/usuario/listar.php");
			}

			var _MetodoRecuperarSenha = function(email){
				return $http.post($rootScope.baseUrl + "php/usuario/recuperar_senha.php", email);
			}

			var _MetodoAlterarSenha = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/alterar_senha.php", obj);
			}

			var _MetodoBuscarHash = function(hash){
				return $http.post($rootScope.baseUrl + "php/usuario/buscar_hash.php", hash);
			}

			var _MetodoBuscarProximoId = function(){
				return $http.post($rootScope.baseUrl + "php/usuario/buscar_proximo_id.php");
			}

			var _MetodoListarTipo = function(){
				return $http.post($rootScope.baseUrl + "php/usuario/listar_tipo.php");
			}

			var _MetodoListarOperadores = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/listar_operadores.php", obj);
			}

			var _MetodoListarOperadoresOrigens = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/listar_operadores_origens.php", obj);
			}

			var _MetodoVerificarVeiculo = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/verificar_veiculo.php", obj);
			}

			var _MetodoVerificarOrigem = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/verificar_origem.php", obj);
			}

			var _MetodoDesvincular = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/desvincular.php", obj);
			}

			var _MetodoDesvincularOrigem = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/desvincular_origem.php", obj);
			}

			var _MetodoVincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/vincular_projeto.php", obj);
			}

			var _MetodoDesvincularProjeto = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/desvincular_projeto.php", obj);
			}

			var _MetodoBaixarQRCode = function(obj){
				return $http.post($rootScope.baseUrl + "php/usuario/gerar_qrcode.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				cadastrar : _MetodoCadastrar,
				excluir : _MetodoExcluir,
				editar : _MetodoEditar,
				buscar : _MetodoBuscar,
				listar: _MetodoListar,
				ativarDesativar : _MetodoAtivarDesativar,
				buscarProximoId : _MetodoBuscarProximoId,
				recuperarSenha : _MetodoRecuperarSenha,
				alterarSenha : _MetodoAlterarSenha,
				buscarHash : _MetodoBuscarHash,
				logar : _MetodoLogar,
				listarTipo : _MetodoListarTipo,
				listarOperadores : _MetodoListarOperadores,
				listarOperadoresOrigens : _MetodoListarOperadoresOrigens,
				verificarVeiculo : _MetodoVerificarVeiculo,
				verificarOrigem : _MetodoVerificarOrigem,
				desvincular : _MetodoDesvincular,
				desvincularOrigem : _MetodoDesvincularOrigem,
				vincularProjeto : _MetodoVincularProjeto,
				desvincularProjeto : _MetodoDesvincularProjeto,
				baixarQRCode: _MetodoBaixarQRCode,
			}
		});
