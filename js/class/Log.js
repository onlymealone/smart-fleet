angular.module("app").factory(
		"Log",
		function($http, $rootScope) {

			var _MetodoListar = function(){
				var obj = {nada: 'nada'}
				return $http.post($rootScope.baseUrl + "php/log/listar.php", obj);
			}

			function error(error) {
				return function() {
					return {
						success : false,
						message : error
					};
				};
			}

			function success(res) {
				return res.data;
			}

			return {
				listar: _MetodoListar
			}
		});
