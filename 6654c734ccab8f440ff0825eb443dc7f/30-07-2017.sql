-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: smartfleet
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_atividade` int(11) DEFAULT NULL,
  `id_tipo_manutencao` int(11) DEFAULT NULL,
  `complemento_manutencao` varchar(40) DEFAULT NULL,
  `transporte_vazio` varchar(80) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `camada`
--

DROP TABLE IF EXISTS `camada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_camada` int(11) NOT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camada`
--

LOCK TABLES `camada` WRITE;
/*!40000 ALTER TABLE `camada` DISABLE KEYS */;
INSERT INTO `camada` VALUES (1,1,0,'Base','2017-06-27 17:36:10','2017-06-27 17:36:10'),(2,1,0,'Sub-Base','2017-06-27 17:36:10','2017-06-27 17:36:10'),(3,1,0,'Capa','2017-06-27 17:36:27','2017-06-27 17:36:27');
/*!40000 ALTER TABLE `camada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razaosocial` varchar(70) DEFAULT NULL,
  `cnpj` varchar(20) DEFAULT NULL,
  `ie` varchar(20) DEFAULT NULL,
  `im` varchar(20) DEFAULT NULL,
  `endereco` varchar(80) DEFAULT NULL,
  `cidade` varchar(40) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `complemento` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Empresa XYZ','000.000-0001/000','IE','IM','Rua teste, jardim homologação','Curitiba','81000-000',NULL,'2017-06-27 17:18:49','2017-06-27 17:18:49');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_carga`
--

DROP TABLE IF EXISTS `local_carga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_carga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_local` int(11) DEFAULT NULL,
  `descricao` varchar(50) DEFAULT NULL,
  `latitude` tinytext,
  `longitude` tinytext,
  `complemento_manual` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_carga`
--

LOCK TABLES `local_carga` WRITE;
/*!40000 ALTER TABLE `local_carga` DISABLE KEYS */;
INSERT INTO `local_carga` VALUES (1,1,1,'Jazida Balança',NULL,NULL,0,'2017-06-27 17:41:34','2017-06-27 17:41:34'),(2,1,2,'Jazida Balança',NULL,NULL,0,'2017-06-27 17:41:34','2017-06-27 17:41:34'),(3,1,1,'Estaca','','',1,'2017-06-27 17:41:50','2017-07-31 08:21:35'),(4,1,2,'Estaca','123','321',0,'2017-06-27 17:41:50','2017-07-31 08:21:31'),(5,1,1,'Polimix',NULL,NULL,0,'2017-06-27 17:42:14','2017-06-27 17:42:14'),(6,1,2,'Polimix',NULL,NULL,0,'2017-06-27 17:42:14','2017-06-27 17:42:14'),(7,0,0,'','','',0,'2017-07-31 07:57:49','2017-07-31 07:57:49'),(8,0,0,'','','',0,'2017-07-31 07:57:55','2017-07-31 07:57:55');
/*!40000 ALTER TABLE `local_carga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_unidade` int(11) DEFAULT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` VALUES (1,1,1,'Massa Asfaltica - CBUQ','2017-06-29 18:24:18','2017-06-27 17:32:22'),(2,1,2,'Argila','2017-06-27 17:32:22','2017-06-27 17:32:22'),(3,1,3,'Areia','2017-06-27 17:32:35','2017-06-27 17:32:35');
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operacao`
--

DROP TABLE IF EXISTS `operacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_veiculo` int(11) DEFAULT NULL,
  `id_operador` int(11) DEFAULT NULL,
  `id_tipo_atividade` int(11) DEFAULT NULL,
  `id_origem` int(11) DEFAULT NULL,
  `id_destino` int(11) DEFAULT NULL,
  `id_origem_material` int(11) DEFAULT NULL,
  `id_camada` int(11) DEFAULT NULL,
  `id_material` int(11) DEFAULT NULL,
  `qtde_volume` int(11) DEFAULT NULL,
  `id_tipo_volume` int(11) DEFAULT NULL,
  `complemento_manutencao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operacao`
--

LOCK TABLES `operacao` WRITE;
/*!40000 ALTER TABLE `operacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `operacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origem_material`
--

DROP TABLE IF EXISTS `origem_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origem_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_origem` int(11) NOT NULL,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origem_material`
--

LOCK TABLES `origem_material` WRITE;
/*!40000 ALTER TABLE `origem_material` DISABLE KEYS */;
INSERT INTO `origem_material` VALUES (1,1,0,'UA-13','2017-06-29 18:09:35','2017-06-29 18:09:35'),(2,1,0,'ES-61','2017-06-29 18:09:35','2017-06-29 18:09:35'),(3,1,0,'ES-69','2017-06-29 18:09:35','2017-06-29 18:09:35');
/*!40000 ALTER TABLE `origem_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_pessoa` int(11) DEFAULT NULL,
  `nome` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `senha` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,1,1,'Guilherme Reis Fernandes','Rua teste, 1900','000.000.000-30','guilherme.fernandes','698dc19d489c4e4db73e28a713eab07b','guilherme@englevel.com.br','(41) 9999-9999','2017-06-27 17:59:23','2017-07-31 03:20:55'),(2,1,2,'Jose Nogueira da Costa Junior','Rua Alphonse Daudet, 219','989.808.123-61','jose.nogueira','698dc19d489c4e4db73e28a713eab07b','jose.costa@actiotech.com.br','(41) 99147-0940','2017-06-28 20:03:31','2017-07-31 04:54:38'),(8,1,2,'Teste','Teste','091.866.199-42','teste','698dc19d489c4e4db73e28a713eab07b','teste@teste','(41) 9999-9999','2017-07-31 05:42:23','2017-07-31 05:42:23');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto`
--

DROP TABLE IF EXISTS `projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `nome` varchar(70) DEFAULT NULL,
  `lote` varchar(20) DEFAULT NULL,
  `extensao` varchar(20) DEFAULT NULL,
  `executor` varchar(80) DEFAULT NULL,
  `cidade` varchar(40) DEFAULT NULL,
  `Estado` varchar(30) DEFAULT NULL,
  `prazo_contratual` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto`
--

LOCK TABLES `projeto` WRITE;
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
INSERT INTO `projeto` VALUES (1,1,'Projeto Jaciara','1A','3000','Guilherme','Jaciara','MS','12 meses','2017-06-27 17:23:35','2017-07-31 02:01:51');
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_atividade`
--

DROP TABLE IF EXISTS `tipo_atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_atividade`
--

LOCK TABLES `tipo_atividade` WRITE;
/*!40000 ALTER TABLE `tipo_atividade` DISABLE KEYS */;
INSERT INTO `tipo_atividade` VALUES (1,'Transporte de material','2017-06-27 17:50:57','2017-06-27 17:50:57'),(2,'Transporte vazio','2017-06-27 17:50:57','2017-06-27 17:50:57'),(3,'Manutencao','2017-06-29 12:51:39','2017-06-27 17:51:19'),(4,'Limpeza','2017-06-27 17:51:19','2017-06-27 17:51:19'),(5,'Chuva','2017-06-27 17:51:29','2017-06-27 17:51:29');
/*!40000 ALTER TABLE `tipo_atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_camada`
--

DROP TABLE IF EXISTS `tipo_camada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_camada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_camada`
--

LOCK TABLES `tipo_camada` WRITE;
/*!40000 ALTER TABLE `tipo_camada` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_camada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_local`
--

DROP TABLE IF EXISTS `tipo_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_local`
--

LOCK TABLES `tipo_local` WRITE;
/*!40000 ALTER TABLE `tipo_local` DISABLE KEYS */;
INSERT INTO `tipo_local` VALUES (1,'Carregamento','2017-06-27 17:38:26','2017-06-27 17:38:26'),(2,'Descarregamento','2017-06-27 17:38:26','2017-06-27 17:38:26');
/*!40000 ALTER TABLE `tipo_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_manutencao`
--

DROP TABLE IF EXISTS `tipo_manutencao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_manutencao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_manutencao`
--

LOCK TABLES `tipo_manutencao` WRITE;
/*!40000 ALTER TABLE `tipo_manutencao` DISABLE KEYS */;
INSERT INTO `tipo_manutencao` VALUES (1,'Troca de pastilha de freio','2017-06-27 17:49:27','2017-06-27 17:49:27'),(2,'Troca de oleo','2017-06-30 18:17:49','2017-06-27 17:49:27'),(3,'Manutencao programada','2017-06-30 18:17:58','2017-06-27 17:49:55'),(4,'Manutencao imprevista','2017-06-30 18:22:31','2017-06-27 17:49:55'),(5,'Outros','2017-06-27 17:50:06','2017-06-27 17:50:06');
/*!40000 ALTER TABLE `tipo_manutencao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_origem`
--

DROP TABLE IF EXISTS `tipo_origem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_origem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_origem`
--

LOCK TABLES `tipo_origem` WRITE;
/*!40000 ALTER TABLE `tipo_origem` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_origem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pessoa`
--

DROP TABLE IF EXISTS `tipo_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pessoa`
--

LOCK TABLES `tipo_pessoa` WRITE;
/*!40000 ALTER TABLE `tipo_pessoa` DISABLE KEYS */;
INSERT INTO `tipo_pessoa` VALUES (1,'Motorista','2017-06-27 17:54:45','2017-06-27 17:54:45'),(2,'Admin. Geral','2017-06-27 17:54:45','2017-06-27 17:54:45'),(3,'Operador','2017-06-27 17:55:00','2017-06-27 17:55:00'),(4,'Admin. Obra','2017-06-27 17:55:00','2017-06-27 17:55:00');
/*!40000 ALTER TABLE `tipo_pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_veiculo`
--

DROP TABLE IF EXISTS `tipo_veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_veiculo`
--

LOCK TABLES `tipo_veiculo` WRITE;
/*!40000 ALTER TABLE `tipo_veiculo` DISABLE KEYS */;
INSERT INTO `tipo_veiculo` VALUES (1,'Caminhao','2017-06-28 20:03:10','2017-06-27 19:11:55'),(2,'Caminhao fora de estrada','2017-06-28 20:03:15','2017-06-27 19:11:55'),(3,'Maquina','2017-06-28 20:03:21','2017-06-27 19:11:55');
/*!40000 ALTER TABLE `tipo_veiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidade`
--

DROP TABLE IF EXISTS `unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidade`
--

LOCK TABLES `unidade` WRITE;
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` VALUES (1,'M²','2017-06-27 17:28:42','2017-06-27 17:28:42'),(2,'Ton','2017-06-27 17:29:06','2017-06-27 17:29:06'),(3,'Kg','2017-06-27 17:29:06','2017-06-27 17:29:06');
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculo`
--

DROP TABLE IF EXISTS `veiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veiculo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(11) DEFAULT NULL,
  `id_tipo_veiculo` int(11) DEFAULT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `placa` varchar(20) DEFAULT NULL,
  `marca` varchar(30) DEFAULT NULL,
  `modelo` varchar(30) DEFAULT NULL,
  `implemento` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculo`
--

LOCK TABLES `veiculo` WRITE;
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` VALUES (1,1,1,'CB-001','GGG-0000','VOLVO','VOLVO',NULL,'2017-06-27 19:24:53','2017-06-27 19:24:53'),(2,1,1,'CB-002','HHH-1111','SCANIA','SCANIA',NULL,'2017-06-27 19:24:53','2017-06-27 19:24:53');
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-31  5:36:46
